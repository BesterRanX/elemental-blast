package gdx.android;

public interface AdHandler {
    void toggleAds();
    void showAds(boolean show);
}
