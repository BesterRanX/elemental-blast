package gdx.android;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import game.Main.GameApp;

public class AndroidLauncher extends AndroidApplication implements AdHandler{

	// ads attributes
	private static String admob_id = "ca-app-pub-6686537470832487/6938173282";
	private final int SHOW_ADS = 1;
	private final int HIDE_ADS = 0;

	// adview attributes
	private static AdView adView;
	private Handler handler;
	private boolean ads_toggle = false;
	protected AdRequest.Builder builder;
	protected RelativeLayout.LayoutParams adParams;

	// relativelayout
	protected RelativeLayout relativeLayout;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// initialise il layout
        layoutInitialization();

        // initialise the gameview
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		View gameView = initializeForView(new GameApp(this, this), config);
		relativeLayout.addView(gameView);

		// initialise the admob
		admobInitialization();
		relativeLayout.addView(adView, adParams);
		adView.loadAd(builder.build());

		//  initialise handerl
		adhandlerInitialization();
		setContentView(relativeLayout);
	}

	public void showAds(boolean show){
		handler.sendEmptyMessage(show ? SHOW_ADS : HIDE_ADS);
	}

	public void toggleAds(){
		showAds(ads_toggle);
		ads_toggle = !ads_toggle;
	}

    private void layoutInitialization(){
		relativeLayout = new RelativeLayout(this);
		relativeLayout.setGravity(Gravity.BOTTOM);
		adParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT
		);
		adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		adParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
	}

	private void admobInitialization(){
		adView = new AdView(this);


		adView.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				int visiblity = adView.getVisibility();
				adView.setVisibility(AdView.GONE);
				adView.setVisibility(visiblity);
				Log.i("admob:","loaded successfully");
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				Log.e("admob:","failed loading");
			}
		});

		adView.setAdSize(AdSize.BANNER);
		adView.setAdUnitId(admob_id);
		builder = new AdRequest.Builder();
		builder.addTestDevice("AC16F03452CDBA7DBA091C5A440827F5");
	}

	private void adhandlerInitialization(){
		handler = new Handler(){
			public void handleMessage(Message msg) {
				switch(msg.what){
					case SHOW_ADS:
						adView.setVisibility(View.VISIBLE);
						break;
					case HIDE_ADS:
						adView.setVisibility(View.GONE);
						break;
				}
			}
		};
	}
}
