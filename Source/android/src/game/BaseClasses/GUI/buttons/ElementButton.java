package game.BaseClasses.GUI.buttons;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import game.BaseClasses.Var.Element;
import game.Main.Compositors.ResourceCache;
import game.Main.Screen.PlayScreen;

/**
 * Created by root on 4/27/17.
 */

public class ElementButton extends Button {
    /************ attributes ************/
    private Element element;
    /************ constructor ***********/
    public ElementButton(final Element _element, final float x, final float y, final float width, final float height){
        super();
        element = _element;
        this.setBounds(x, y, width, height);
        this.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {actionByElement();}
        });
        initialiseStyle();
    }

    /************ OPERATORS *************/
    private void initialiseStyle(){
        ButtonStyle buttonStyle = new ButtonStyle();
        buttonStyle.up = new TextureRegionDrawable(ResourceCache.framesList.get("elementbutton_"
                +getNameByElement()+"_UP").first());
        buttonStyle.down = new TextureRegionDrawable(ResourceCache.framesList.get("elementbutton_"
                +getNameByElement()+"_DOWN").first());
        this.setStyle(buttonStyle);

    }

    private void actionByElement(){
        PlayScreen.gameManager.wisp.switchElement(element);
    }

    private String getNameByElement(){
        switch (element){
            case FIRE: return "FIRE";
            case WATER: return "WATER";
            case NATURE: return "NATURE";
            case EARTH:return "EARTH";
            default:return "NEUTRAL";
        }
    }
    /************ methods **************/
}

