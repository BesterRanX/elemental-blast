package game.BaseClasses.GUI.gameUI;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import game.Main.GameApp;
import game.Main.Screen.PlayScreen;

/**
 * Created by root on 11/10/17.
 */

public class ExitButton extends TextButton {
    /***************** ATTRIBUTES ******************/
    GameApp gameApp;
    /***************** CONSTRUCTOR *******************/
    public ExitButton(String text, TextButtonStyle style, final GameApp gapp){
        super(text, style);
        gameApp = gapp;

        addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                PlayScreen.guiManager.scoreLabel.checkRecord();
                GameApp.playScreen.dispose();
                gameApp.getMainScreenInstance().resume();
                gameApp.adHandler.toggleAds();
                gameApp.setScreen(gameApp.getMainScreenInstance());
            }
        });
    }
}
