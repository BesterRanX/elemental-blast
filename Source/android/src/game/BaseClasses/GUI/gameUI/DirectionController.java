package game.BaseClasses.GUI.gameUI;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;

import game.BaseClasses.Var.GameVar;
import game.Main.Compositors.ResourceCache;

import static game.BaseClasses.Var.GameVar.GENERATOR_HEIGHT;

/**
 * Created by root on 4/29/17.
 */

public class DirectionController extends Actor{
    /************** ATTRIBUTES **************/
    private int padding = 300;
    private TextureRegion currentFrame;
    private float lastX;
    private float lastRotation;
    private float velocity = 1.85f;
    private Vector2 position;

    /************* CONSTRUCTOR *************/
    public DirectionController(float posx, float posy, float width, float height, float _rotation){
        position = new Vector2(posx, posy);
        this.setBounds(position.x - padding/2, position.y, width + padding, height);
        setRotation(_rotation);
        this.setTouchable(Touchable.enabled);

        /*---------- set bounds properties -----------*/

        /*----------- center of rotation ----------*/
        this.setOrigin(width/2, -GENERATOR_HEIGHT/2);

        this.addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                lastX = x;
                return true;
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                super.touchDragged(event, x, y, pointer);
                // la distanza trascinata
                float distance = x - lastX;

                // l'ultimo grado meno il arctan della lunghezza trascinata in ascissa.
                setRotation(getRotation() - (float)Math.atan(distance)*velocity);

                stabilizeDegree(distance);

                // verifica che la rotazione della freccia sia nei limiti stabiliti.
                limitDegreeRange();

                normalizeDegree();
                lastRotation = getRotation();
            }
        });
        initialiseTextures();
    }


    /*************** OPERATORS ****************/
    private void stabilizeDegree(float distance){
        if (distance <= 10 && distance >= -10) setRotation(lastRotation);
    }

    private void limitDegreeRange(){
        if (getRotation() < -40) setRotation(-40);
        else if (getRotation() > 40) setRotation(40);
    }

    private void normalizeDegree(){
        if (getRotation() < -360) setRotation(getRotation() + 360);
        else if (getRotation() > 360) setRotation(getRotation() - 360);
    }

    private void initialiseTextures(){
        currentFrame = ResourceCache.framesList.get("directioncontroller_STANDBY").first();
    }


    /******************** METHODS ********************/
    @Override
    public void draw(Batch batch, float alpha){
        batch.draw(currentFrame, position.x, position.y, getOriginX(), getOriginY(),
                GameVar.D_CONTROLLER_WIDTH, GameVar.D_CONTROLLER_HEIGHT, 1, 1, getRotation());
    }
}
