package game.BaseClasses.GUI.gameUI;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import game.Main.Screen.PlayScreen;

/**
 * Created by root on 5/20/17.
 */

public class TimeLabel extends Label {
    /*************** ATTRIBUTES *****************/
    private int difficulty_cap = 60;
    private int difficulty_counter = 0;

    private int seconds = 0;
    private int minutes = 0;
    private float amount = 0;


    /**************** CONSTRUCTOR *************************/
    public TimeLabel(LabelStyle style){
        super("", style);
    }


    /***************** METHODS ********************/
    @Override
    public void act(float delta) {
        super.act(delta);
        amount += delta*100;

        /*------ calculate seconds ------*/
        if (amount >= 60) {
            seconds += 1;
            amount = 0;
        }

        /*----- calculate minutes ----*/
        if (seconds >= 60) {
            seconds = 0;
            minutes += 1;
            difficulty_counter += 1;
        }

        /*---------- each 80s increase the game difficulty ----------*/
        if (difficulty_counter >= difficulty_cap){
            difficulty_counter = 0;
            difficulty_cap += 45;
            PlayScreen.gameManager.increaseDifficult();
        }

        setText("Time: "+String.format("%2d", minutes)+"."+String.format("%2d",seconds));
        System.out.println(difficulty_counter);
    }
}
