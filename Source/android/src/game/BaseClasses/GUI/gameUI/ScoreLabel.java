package game.BaseClasses.GUI.gameUI;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import game.BaseClasses.Var.GameVar;
import game.Main.GameApp;

/**
 * Created by root on 5/20/17.
 */

public class ScoreLabel extends Label {
    private int score = 0;
    private int amount = 0;
    private String name;

    /************* CONSTRUCTOR *****************/
    public ScoreLabel(LabelStyle style, String labelname){
        super("", style);
        name = labelname;
        setText(name + ": "+String.format("%3d",score));
    }


    /*************** OPERATORS ***************/
    private void transferAmount(){
        if (amount > 0){
            amount -= 1;
            score += 1;
            setText(name + ": "+String.format("%3d",score));
        }
    }

    /*************** METHODS *****************/
    public void increaseScore(int s){
        amount += s;
    }

    public void setScore(int s){
        score = s;
        setText(name + ": "+String.format("%3d",score));
        Log.i("set score:","score = "+s);
    }

    public void checkRecord(){
        int last_record = readScore();
        if(score > last_record){
            archiveScore();
        }
    }

    public void archiveScore(){
        try{
            FileOutputStream fos = GameApp.GetContext().openFileOutput(GameVar.FILE_RECORD, Context.MODE_PRIVATE);
            fos.write(score);
            fos.close();
            Log.i("archive score:", "score wrote "+score);
        }catch(IOException e){
            Log.e("archive score:", "writing score error");
        }
    }

    public int readScore(){
        int record = -1;
        try{
            FileInputStream fis = GameApp.GetContext().openFileInput(GameVar.FILE_RECORD);
            record = fis.read();
            fis.close();
        }catch (IOException e){
            Log.e("read record:", "cannot read the record");
        }
        Log.i("read record:","record = "+record);
        return record;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        transferAmount();
    }
}
