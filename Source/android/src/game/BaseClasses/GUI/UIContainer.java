package game.BaseClasses.GUI;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;

import game.BaseClasses.Actors.FrameObject;

/**
 * Created by root on 4/27/17.
 */

public class UIContainer extends Stage {
    private ArrayList<FrameObject> frames;
    /************ constructor ************/
    public UIContainer(Viewport _viewport, Batch _batch){
        super(_viewport, _batch);
        frames = new ArrayList();
    }

    @Override
    public void draw(){
        super.draw();
        getBatch().begin();
        for(FrameObject frame : frames){
            getBatch().draw(frame.getCurrentFrame(), frame.position.x, frame.position.y, frame.getWidth(), frame.getHeight());
        }
        getBatch().end();
    }

    public void addFrame(FrameObject _frame){
        frames.add(_frame);
    }
}
