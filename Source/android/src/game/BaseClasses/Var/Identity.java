package game.BaseClasses.Var;

import java.util.Random;

import static game.BaseClasses.Var.Element.EARTH;
import static game.BaseClasses.Var.Element.FIRE;
import static game.BaseClasses.Var.Element.NATURE;
import static game.BaseClasses.Var.Element.NEUTRAL;
import static game.BaseClasses.Var.Element.WATER;

/**
 * Created by root on 4/24/17.
 */

public class Identity {
    /************** ATTRIBUTES ******************/
    public short bit_category;
    public Element element;

    /*************** CONSTRUCTOR ****************/
    public Identity(short bit_category, Element element){
        this.bit_category = bit_category;
        this.element = element;
    }

    public Identity(short bit_category){
        this.bit_category = bit_category;
        this.element = randomElement();
    }

    /**************** METHODS *******************/
    public boolean counters(Identity target_identity){
        switch (element){
            case FIRE: {
                if (target_identity.element == Element.NATURE) return true;
                else return false;
            }

            case NATURE: {
                if (target_identity.element == Element.EARTH) return true;
                else return false;
            }

            case WATER:{
                if (target_identity.element == FIRE) return true;
                else return false;
            }
            case EARTH: {
                if (target_identity.element == Element.WATER) return true;
                else return false;
            }
            default:return false;
        }
    }

    public String getElementToString(Element _element){
        switch (_element){
            case FIRE: return "FIRE";
            case WATER: return "WATER";
            case NATURE: return "NATURE";
            case EARTH: return "EARTH";
            default: return "NEUTRAL";
        }
    }

    public static Element randomElement(){
        Random rand = new Random();
        switch (rand.nextInt(4)+1){
            case 1: return FIRE;
            case 2: return WATER;
            case 3: return NATURE;
            case 4: return EARTH;
            default:return NEUTRAL;
        }
    }
}
