package game.BaseClasses.Var;

/**
 * Created by root on 4/24/17.
 */

public enum Status {
    STANDBY, EXPLOSION, SHOOT, DEAD, SHAKE, REGENERATING
}
