package game.BaseClasses.ToolKits;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Shape2D;

/**
 * Created by root on 5/7/17.
 */

public abstract class Effect {
    protected Shape2D shape;
    protected Sound effectSound;
    public Effect(Shape2D shape){
        this.shape = shape;
    }

    public abstract void applyEffect(float delta);
}
