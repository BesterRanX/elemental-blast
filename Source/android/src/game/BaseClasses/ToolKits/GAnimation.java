package game.BaseClasses.ToolKits;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.utils.Array;

import game.Main.Compositors.ResourceCache;

/**
 * Created by root on 4/22/17.
 */

public class GAnimation<Type> extends Animation<Type>{
    /*************** ATTRIBUTES ***********/
    private float stateTime = 0;
    public boolean isFinished = false;
    private boolean firstAnimation = true;
    private int currentFrame = 0;

    /*************** CONSTRUCTOR ***************/
    public GAnimation(float frameDuration, Array keyFrames, PlayMode playMode) {
        super(frameDuration, keyFrames, playMode);
    }


    /**************** OPERATORS **************/
    private void reset(){
        firstAnimation = true;
        isFinished = false;
        stateTime = 0;
    }


    /******************* METHODS *******************/
    public int getCurrentFrame(){return currentFrame;}

    public int getFrameLength(){return getKeyFrames().length;}

    public boolean isFrame(int i){
        if (getKeyFrameIndex(stateTime) == i) return true;
        else return false;
    }

    /* return the respective animation frame by the statetime
     * statetime is a float value < 1(second)*/
    public Type play(final float delta){
        if (firstAnimation) firstAnimation = !firstAnimation;
        else stateTime += delta;

        /* switch by playmode */
        if (getPlayMode() == PlayMode.NORMAL) return playNormal();
        else if (getPlayMode() == PlayMode.LOOP) return playLoop();
        else return (Type) ResourceCache.framesList.get("VOID_VOID").first();
     }

    private Type playLoop(){
        /*--------- when finishes the animation, reset it ------*/
        if (isAnimationFinished(stateTime)) stateTime = 0;
        currentFrame = getKeyFrameIndex(stateTime);
        return getKeyFrame(stateTime);
    }

    private Type playNormal(){
        isFinished = isAnimationFinished(stateTime);
        if (!isFinished){
            currentFrame = getKeyFrameIndex(stateTime);
            return getKeyFrame(stateTime);
        }
        /*-------- when it's finished return an empty texture(image) ---------*/
        else {
            /*--------- reset ---------*/
            firstAnimation = true;
            stateTime = 0;
            return (Type) ResourceCache.framesList.get("VOID_VOID").first();
        }
    }
}
