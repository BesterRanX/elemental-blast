package game.BaseClasses.ToolKits;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;

import java.io.BufferedReader;
import java.util.HashMap;

/**
 * Created by root on 5/11/17.
 */

public class SoundLoader{
    /************* attributes **************/
    private HashMap<String, Sound> soundList;

    /************** constructor *************/
    public SoundLoader(FileHandle file){
        soundList = new HashMap<String, Sound>();
        loadResources(file);
    }

    public SoundLoader(){
        soundList = new HashMap<String, Sound>();
    }

    /************** SETTING/GETTING *************/
    public HashMap getSounds(){return soundList;}

    /*************** OPERATORS *****************/
    public void loadResources(final FileHandle file){
        BufferedReader reader = new BufferedReader(file.reader());
        String line = "";
        try{
            while(!(line = reader.readLine()).isEmpty()) parseLine(line);
        }catch (Exception ex){}
    }

    /* parseLine splits the line into an array of words.
       Then read the array: if it finds the "=" then read the sound file by 'path'.
     */
    private void parseLine(String line){
        String[] splittedLine = line.split(" ");
        String name = "";
        String path = "";
        for (int i = 0; i < splittedLine.length; i++){
            if (splittedLine[i].equals("=")){
                name = splittedLine[i-1];
                path = splittedLine[i+1];
            }
        }
        if (!name.isEmpty() && !path.isEmpty()) soundList.put(name, Gdx.audio.newSound(Gdx.files.internal(path)));
    }
}
