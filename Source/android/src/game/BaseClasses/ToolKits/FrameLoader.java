package game.BaseClasses.ToolKits;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;

import java.io.BufferedReader;
import java.util.HashMap;

/**
 * Created by root on 5/11/17.
 */

public class FrameLoader {
    /************* ATTRIBUTES **************/
    private HashMap<String, Array<AtlasRegion>> frameList;


    /************ CONSTRUCTOR **************/
    public FrameLoader(FileHandle file){
        frameList = new HashMap<String, Array<AtlasRegion>>();
        loadResources(file);
    }


    public FrameLoader(){
        frameList = new HashMap<String, Array<AtlasRegion>>();
    }

    /************ SETTING/GETTING **********/
    public HashMap<String, Array<AtlasRegion>> getFrameList() {return frameList;}


    /************* OPERATORS *************/
    public void loadResources(FileHandle file){
        BufferedReader bufferedFile = new BufferedReader(file.reader());
        /* ------ Input operation ----*/
        String line = "";
        try{
            while(!(line = bufferedFile.readLine()).isEmpty()) parseLine(line);
        }catch (Exception ex){}
    }

    private void parseLine(String line){
        /*---------- parsed variables --------*/
        String _name = "";
        String _path = "";
        Array<String> _animations = new Array<String>();

        /*---------- split the line into worlds -----*/
        String word[] = line.split(" ");

        /*------- read words ------*/
        for (int i = 0; i < word.length; i++){
            //if it's a comment
            if (word[i].equals("#") || word[i].contains("#")) return;
            //if it's an assignment
            if (word[i].equals("=")){
                _name = word[i-1];
                _path = word[i+1];
            }
            //if it's an animation
            if (word[i].equals(":")){
                _animations.add(word[i+1]);
            }
        }

        if (!_name.isEmpty() && !_path.isEmpty()) createAnimation(_name, _path, _animations);
    }

    private void createAnimation(String _name, String _path, Array<String> _animations){
        TextureAtlas tmpAtlas = new TextureAtlas(Gdx.files.internal(_path));
        /*------------ create animation with "name + animation_name"
         * animation name can be more than 1. So it's an array.
        */
        for (String animation : _animations){
            frameList.put(_name +"_"+ animation, tmpAtlas.findRegions(animation));
        }
    }
}
