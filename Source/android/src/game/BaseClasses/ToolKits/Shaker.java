package game.BaseClasses.ToolKits;

import com.badlogic.gdx.graphics.OrthographicCamera;

import java.util.Random;

/**
 * Created by root on 4/26/17.
 */

public class Shaker {
    private float shake_duration, shake_frequency;
    private float accumulatedTime;
    private float currentTime;
    private int min_shake, max_shake;
    private int current_shake_X;
    private int current_shake_Y;
    private Random rand;
    private boolean isFinished;

    /************** constructor *************/
    public Shaker(){
        rand = new Random();
        reset();
        shake_duration = 0;
        shake_frequency = 0;
    }

    public Shaker(int min_shake, int max_shake, float shake_duration, float shake_frequency){
        rand = new Random();
        reset();
        this.min_shake = min_shake;
        this.max_shake = max_shake;
        this.shake_duration = shake_duration;
        this.shake_frequency = shake_frequency;
    }

    /************* setting/getting **********/
    public boolean isFinished(){return isFinished;}
    public void setFrequency(float frequency){
        shake_frequency = frequency;
    }

    /*************** methods ***************/
    public void shake(OrthographicCamera camera, float delta){
        if (accumulatedTime <= shake_duration){
            accumulatedTime += delta;
            applyFrequency(camera, delta);

        } else {
            reset();
            isFinished = true;
        }
    }

    public void extendShakeDuration(float duration){
        this.shake_duration += duration;
    }

    /***************** operators **************/
    private void applyFrequency(OrthographicCamera camera, float delta){
        currentTime += delta;
        if (currentTime >= shake_frequency){
            int randX = getRandomShake(current_shake_X);
            int randY = getRandomShake(current_shake_Y);
            camera.translate(randX, randY);
            calculateShake(randX, randY);
            currentTime -= shake_frequency;
        }
    }

    private void calculateShake(int randx, int randy){
        current_shake_X += randx;
        current_shake_Y += randy;
    }

    private void reset(){
        current_shake_X = 0;
        current_shake_Y = 0;
        currentTime = 0;
        accumulatedTime = 0;
        isFinished = false;
    }

    private int getRandomShake(int currenShake){
        int randomNum = rand.nextInt(Math.abs(min_shake)+max_shake)-min_shake;
        if ((currenShake >= max_shake && randomNum > 0) ||
            (currenShake <= min_shake && randomNum < 0)){

            return negateNumber(randomNum);
        }
        else return randomNum;
    }

    private int negateNumber(int number){
        return number *= -1;
    }
}
