package game.BaseClasses.ToolKits;

import com.badlogic.gdx.math.Shape2D;

import game.BaseClasses.Actors.Bomb;
import game.BaseClasses.Actors.GameObject;
import game.BaseClasses.Var.Identity;
import game.Main.Compositors.ResourceCache;
import game.Main.GameApp;
import game.Main.Screen.PlayScreen;

/**
 * Created by root on 5/7/17.
 */

public class ExplosionEffect extends Effect {
    private Identity identity;
    public static boolean isChainExplosion = false;
    /****************** constructor *******************/
    public ExplosionEffect(Identity identity, Shape2D shape){
        super(shape);
        this.identity = identity;
        effectSound = ResourceCache.soundsList.get("explosion");
    }

    @Override
    public void applyEffect(float delta) {
        effectSound.play(GameApp.sound_volume);
        for (GameObject go : PlayScreen.gameManager.bombsList){
            Bomb bomb = (Bomb)go;
            /* explode if the explosion's element counters bomb's element,
               and the bomb is in the effect's shape */
            if (this.identity.counters(bomb.identity) &&
                    shape.contains(bomb.position.x, bomb.position.y)){
                bomb.explode();
                isChainExplosion = true;
            }
        }
        if(!isChainExplosion) PlayScreen.gameManager.wisp.regenerate();
    }
}
