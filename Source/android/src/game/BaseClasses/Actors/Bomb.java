package game.BaseClasses.Actors;

import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import game.BaseClasses.ToolKits.ExplosionEffect;
import game.BaseClasses.ToolKits.GAnimation;
import game.BaseClasses.Var.Element;
import game.BaseClasses.Var.GameVar;
import game.BaseClasses.Var.Identity;
import game.BaseClasses.Var.Status;
import game.Main.Compositors.ResourceCache;
import game.Main.Screen.PlayScreen;

import static game.BaseClasses.Var.GameVar.PPM;

/**
 * Created by root on 4/16/17.
 */

public class Bomb extends Sprite {
    /**************** ATTRIBUTES ***************/
    /*--------- animations ----------*/
    private GAnimation<AtlasRegion> explosion;
    private TextureRegion standby;

    private final float animationFPS = 0.035f;//animation's duration is 14FPS
    private final PlayMode playMode = PlayMode.NORMAL;


    /**************** CONSTRUCTOR *************/
    public Bomb(final Element element, final World world, final float posx, final float posy, final int width, final int height){
        super(new Identity(GameVar.BIT_BOMB, element), world, posx, posy, width, height);

        //------ define bomb body
        createBody(BodyDef.BodyType.StaticBody);

        //------- define circle shape and fixture
        CircleShape cshape = new CircleShape();//create a circle shape
        /*----- create shape as pixels per meter ---*/
        cshape.setRadius((width/2)/PPM);
        FixtureDef fdef = new FixtureDef();
        fdef.shape = cshape;
        fdef.filter.categoryBits = GameVar.BIT_BOMB;
        fdef.filter.maskBits = GameVar.BIT_WISP;
        createFixture(fdef);
        cshape.dispose();
        //------- define EXPLOSION animation
        initialiseTexture(identity.element);
    }


    /********* SETTING/GETTING ***********/
    public void setCurrentFrame(AtlasRegion ta){
        currentFrame = ta;
    }
    public void setStatus(Status _status){this.status = _status;}


    /************* OPERATORS ***********/
    private void initialiseTexture(Element element){
        String elementName = identity.getElementToString(element).toLowerCase();
        standby = ResourceCache.framesList.get(elementName+"bomb_STANDBY").first();
        explosion = new GAnimation<AtlasRegion>(animationFPS,
                ResourceCache.framesList.get(elementName+"bomb_EXPLOSION"), playMode);
    }

    /* ---------- action handlers --------- */
    private void explosion(final float delta){
        /*----- play explosion animation -----*/
        currentFrame = explosion.play(delta);

        /*---- when the animation reaches an half apply explosion effect ...*/
        if (explosion.isFrame(explosion.getFrameLength()/2)) {
            ExplosionEffect effect = new ExplosionEffect(this.identity, new Circle(position.x, position.y, 100));
            effect.applyEffect(delta);
        }
        /*----- when the animation is finished dispose the object ------*/
        if (explosion.isFinished) {
            status = Status.DEAD;
            PlayScreen.guiManager.scoreLabel.increaseScore(10);

            if(!PlayScreen.gameManager.isExploding()) {
                PlayScreen.gameManager.wisp.regenerate();
                PlayScreen.gameManager.applyLevelEffect();
            }
        }
    }

    /******************** METHODS *****************/
    public void explode(){
        if (!status.equals(Status.DEAD)) {
            setStatus(Status.EXPLOSION);
            PlayScreen.gameCamera.shake(0.3f, 0.03f);
        }
    }

    public void standby(){currentFrame = standby;}

    @Override
    public void update(float delta) {
        if (status.equals(Status.DEAD)) dispose();
        if (status.equals(Status.REGENERATING) && body.getPosition().y <= 64){
            body.setLinearVelocity(0,0);
            setStatus(Status.STANDBY);
        }

        position.set(body.getPosition().x * PPM, body.getPosition().y * PPM);
    }

    @Override
    public void draw(final Batch batch, final float delta) {
        handleStatusDrawing(delta);
        batch.draw(currentFrame, position.x, position.y, GameVar.BOMB_WIDTH, GameVar.BOMB_HEIGHT);
    }

    @Override
    public void onCollideBegins(Identity targetIdentity) {
    }

    @Override
    public void onCollideEnds(Identity targetIdentity){}

    @Override
    protected void handleStatusDrawing(float delta){
        switch (status){
            case STANDBY:
                standby();
                return;

            case EXPLOSION:
                explosion(delta);
                return;
        }
    }

    public void dispose(){
        world.destroyBody(body);
        PlayScreen.gameManager.remove(this);
    }
}
