package game.BaseClasses.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import game.BaseClasses.ToolKits.ExplosionEffect;
import game.BaseClasses.ToolKits.GAnimation;
import game.BaseClasses.Var.Element;
import game.BaseClasses.Var.GameVar;
import game.BaseClasses.Var.Identity;
import game.BaseClasses.Var.Status;
import game.Main.Compositors.ResourceCache;
import game.Main.Screen.PlayScreen;

import static game.BaseClasses.Var.GameVar.BIT_BOMB;
import static game.BaseClasses.Var.GameVar.BIT_WISP;
import static game.BaseClasses.Var.GameVar.PPM;

/**
 * Created by root on 4/16/17.
 */

public class Wisp extends Sprite {
    /*************** ATTRIBUTES *****************/
    private boolean exploded = false;
    /*-------- animations and texture --------*/
    private GAnimation<AtlasRegion> standby, explosion;
    private AtlasRegion ballform;


    /*************** CONSTRUCTOR ****************/
    public Wisp(World world, float posx, float posy, float width, float height){
        super(new Identity(BIT_WISP, Element.NEUTRAL), world, posx, posy, width, height);
        //------ define wisp body
        createBody(BodyDef.BodyType.DynamicBody);//affected by forces
        //------ define wisp fixture (pixels per meter)
        CircleShape cshape = new CircleShape();//create a circle shape for fixture
        cshape.setRadius((height/3.5f)/PPM);
        FixtureDef fdef = new FixtureDef();
        fdef.shape = cshape;
        fdef.filter.categoryBits = BIT_WISP;
        fdef.filter.maskBits = BIT_BOMB;
        createFixture(fdef);
        cshape.dispose();

        initialiseTexture();
    }


    /************** OPERATORS **************/
    private void initialiseTexture(){
        standby = new GAnimation<AtlasRegion>(0.08f, ResourceCache.framesList.get("wisp_STANDBY"), PlayMode.LOOP);
    }

    private void checkScreenBoundary(){
        if( position.x > Gdx.graphics.getWidth() || position.x < 0) {
            body.setAwake(false);
            regenerate();
        }
    }

    /* ---------- action handlers ---------*/
    private void standby(final float delta){currentFrame = standby.play(delta);}
    private void ballform(){currentFrame = ballform;}
    private void dead(){
        currentFrame = ResourceCache.framesList.get("VOID_VOID").first();
        regenerate();
    }

    private void explosion(final float delta){
        currentFrame = explosion.play(delta);

        if(explosion.getCurrentFrame() >= 4 && !exploded) {
            ExplosionEffect effect = new ExplosionEffect(this.identity, new Circle(position.x, position.y, 120));
            effect.applyEffect(delta);
            exploded = true;
        }
        if (explosion.isFinished) status = Status.DEAD;
    }


    /************************ METHODS ****************************/
    /*---------- element transformation -----------*/
    public void switchElement(final Element _element){
        if (status.equals(Status.STANDBY)){
            String elementName = identity.getElementToString(_element).toLowerCase();
            identity.element = _element;
            /*-------- switch animations -------*/
            ballform = ResourceCache.framesList.get(elementName+"wisp_BALL").first();
            standby = new GAnimation<AtlasRegion>(0.08f, ResourceCache.framesList.get(elementName+"wisp_STANDBY"), PlayMode.LOOP);
            explosion = new GAnimation<AtlasRegion>(0.03f, ResourceCache.framesList.get(elementName+"wisp_EXPLOSION"), PlayMode.NORMAL);
        }
    }

    public void shoot(final float angle){
        if (!identity.element.equals(Element.NEUTRAL)){
            status = Status.SHOOT;
            float toRadians = (float)Math.toRadians(angle + 90);
            float forceX = (float)Math.cos(toRadians);
            float forceY = (float)Math.sin(toRadians);
            body.applyLinearImpulse(forceX * 100, forceY * 100, body.getPosition().x, body.getPosition().y, true);
        }
    }

    /*-------- return to it's original position with "regenerating" animation -------*/
    public void regenerate(){
        body.setTransform(GameVar.START_X / PPM, GameVar.START_Y / PPM, 0);
        //da cambiare
        status = Status.STANDBY;
        body.setAwake(true);
        exploded = false;
    }

    /************ overrided methods **************/
    @Override
    public void dispose(){
        PlayScreen.gameManager.remove(this);
        world.destroyBody(body);
    }

    @Override
    public void update(final float delta){
        position.set(body.getPosition().x * PPM, body.getPosition().y * PPM);
        checkScreenBoundary();
    }

    @Override
    public void draw(final Batch batch, final float delta){
        handleStatusDrawing(delta);
        batch.draw(currentFrame, position.x, position.y, width, height);
    }

    @Override
    public void onCollideBegins(final Identity target_identity) {
        /*-------- do something when collides the bomb ------*/
        status = Status.EXPLOSION;
    }

    @Override
    public void onCollideEnds(Identity identity){
        body.setAwake(false);
    }

    @Override
    protected void handleStatusDrawing(final float delta) {
        switch (status){
            case STANDBY:
                standby(delta);
                return;
            case SHOOT:
                ballform();
                return;
            case EXPLOSION:
                explosion(delta);
                return;
            case DEAD:
                dead();
                return;
        }
    }
}
