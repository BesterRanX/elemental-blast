package game.BaseClasses.Actors;

import com.badlogic.gdx.graphics.g2d.Batch;

/**
 * Created by root on 4/16/17.
 */

public interface GameObject {
    public void update(float delta);
    public void draw(Batch batch, float delta);
    public void dispose();
}
