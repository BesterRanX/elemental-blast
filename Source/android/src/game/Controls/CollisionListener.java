package game.Controls;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

import game.BaseClasses.Actors.Sprite;

/**
 * Created by root on 4/24/17.
 */

public class CollisionListener implements ContactListener {

    /**************** OPERATORS ***************/
    private void handleCollisionBegins(Sprite objectA, Sprite objectB){
        objectA.onCollideBegins(objectB.identity);
        objectB.onCollideBegins(objectA.identity);
    }

    private void handleCollisionEnds(Sprite objectA, Sprite objectB){
        objectA.onCollideEnds(objectB.identity);
        objectB.onCollideEnds(objectA.identity);
    }


    /*************** METHODS ******************/
    @Override
    public void beginContact(Contact contact) {
        handleCollisionBegins((Sprite)contact.getFixtureA().getUserData(),
                (Sprite)contact.getFixtureB().getUserData());
    }
    @Override
    public void endContact(Contact contact) {
        handleCollisionEnds((Sprite)contact.getFixtureA().getUserData(),
                (Sprite)contact.getFixtureB().getUserData());
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {}
    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {}
}
