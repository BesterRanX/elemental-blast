package game.Main;

import android.content.Context;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.Main.Compositors.ResourceCache;
import game.Main.Screen.MainScreen;
import game.Main.Screen.PlayScreen;
import gdx.android.AdHandler;

public class GameApp extends Game implements ApplicationListener {
	// application context
	private static Context context;
    public AdHandler adHandler;

	//********** static attributes ****
	public static float music_volume = 1f;
	public static float sound_volume = 0.4f;

	/* ***** screens ***** */
    private MainScreen mainScreen;
	public static PlayScreen playScreen;

    /* **** resources **** */
    public ResourceCache RC;
	public SpriteBatch batch;

	/************** COSTRUCTOR *************/
    public GameApp(Context cont, AdHandler ad){
    	context = cont;
    	adHandler = ad;
	}

	@Override
	public void create () {
		batch = new SpriteBatch();
		RC = new ResourceCache();

		mainScreen = new MainScreen(this);
		setScreen(mainScreen);
	}

	public MainScreen getMainScreenInstance(){
		return mainScreen = new MainScreen(this);
	}

	public PlayScreen getPlayScreenInstance(){
		playScreen = new PlayScreen(this);
		return playScreen;
	}

	public static Context GetContext(){
    	return context;
	}

	@Override
	public void render () {
		super.render();
	}
	@Override
	public void dispose () {
		batch.dispose();
	}
}
