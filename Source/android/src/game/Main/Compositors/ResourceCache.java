package game.Main.Compositors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;

import game.BaseClasses.ToolKits.FrameLoader;
import game.BaseClasses.ToolKits.SoundLoader;

/**
 * Created by root on 4/15/17.
 */

public class ResourceCache {
    /********* cached resources *********/
    public static HashMap<String, Array<AtlasRegion>> framesList;
    public static HashMap<String, Sound> soundsList;

    /*********** attributes ************/
    private SoundLoader soundLoader;
    private FrameLoader frameLoader;

    //*********** constructor **********/
    public ResourceCache(){
        frameLoader = new FrameLoader();
        soundLoader = new SoundLoader();

        framesList = new HashMap<String, Array<AtlasRegion>>();
        soundsList = new HashMap<String, Sound>();

        loadFrames();
        loadSounds();
    }


    /************* OPERATORS **************/

    private void loadFrames(){
        frameLoader.loadResources(Gdx.files.internal("AnimationList"));
        frameLoader.loadResources(Gdx.files.internal("GuiList"));
        framesList.putAll(frameLoader.getFrameList());
    }

    private void loadSounds(){
        soundLoader.loadResources(Gdx.files.internal("SoundList"));
        soundsList.putAll(soundLoader.getSounds());
    }
}
