package game.Main.Compositors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import java.io.BufferedReader;
import java.io.IOException;

import game.BaseClasses.Actors.Bomb;
import game.BaseClasses.Actors.GameObject;
import game.BaseClasses.Actors.Sprite;
import game.BaseClasses.Actors.Wisp;
import game.BaseClasses.Var.Element;
import game.BaseClasses.Var.GameVar;
import game.BaseClasses.Var.Identity;
import game.BaseClasses.Var.Status;

import static game.BaseClasses.Var.GameVar.PPM;

/**
 * Created by root on 4/18/17.
 */

public class GameManager {
    /*************** ATTRIBUTES ******************/
    //----- core variables
    private World world;//reference to the world
    private int level = 0;
    public Texture background ;

    //----- game objects
    public Array<GameObject> gameObjects;
    public Array<GameObject> bombsList;
    public static Wisp wisp;
    public Music background_music;


    /**************** CONSTRUCTOR ***************/
    public GameManager(World world){
        //get reference
        this.world = world;
        bombsList = new Array<GameObject>();
        gameObjects = new Array<GameObject>();
        background_music = Gdx.audio.newMusic(Gdx.files.internal("Sounds/background_music/game.mp3"));
    }


    /***************** OPERATORS *****************/
    private void createWisp(){
        wisp = new Wisp(world, GameVar.START_X, GameVar.START_Y, GameVar.WISP_WIDTH, GameVar.WISP_HEIGHT);
        gameObjects.add(wisp);
    }

    private void clearAll(){
        for(GameObject go : gameObjects){
            go.dispose();
        }
        gameObjects.clear();
    }

    private void parseGameObjects(BufferedReader buffer) throws IOException{
        String line;
        float posx = GameVar.LEFT_EDGE;
        float posy = GameVar.TOP_EDGE;

        /*------- read line by line --------*/
        while (!(line = buffer.readLine()).isEmpty()){
            String[] lineWords = line.split(" ");

            /*--------- parse each word --------*/
            for (int i = 0; i < lineWords.length; i++){
                createObjectById(lineWords[i], posx, posy);
                posx += (64 + GameVar.PADDING_X);
            }
            posx = GameVar.LEFT_EDGE;
            posy -= (64 + GameVar.PADDING_Y);
        }
    }

    private void createObjectById(String id, float posx, float posy){
        if (id.equals("frb") || id.equals("wtb") || id.equals("ntb") || id.equals("etb"))
            createObject(id, posx, posy);
    }

    private void parseProperties(BufferedReader bufferedfile) throws IOException{
        String line;
        boolean gameobjects = false;
        while(!gameobjects && !(line = bufferedfile.readLine()).isEmpty()){
            /*------- temp cache ------*/
            String[] word = line.split(" ");
            for (int i = 0; i < word.length; i++){
                if (word[i].equals(":")){
                    if (word[i-1].equals("Index")) level = Integer.parseInt(word[i+1]);
                    if (word[i-1].equals("Background")) background = new Texture("GUI/backgrounds/"+ word[i+1]);
                    if (word[i-1].equals("GameObjects")) gameobjects = true;
                }
            }
        }
    }

    private void parseFile(BufferedReader bufferedFile){
        try{
            parseProperties(bufferedFile);
            parseGameObjects(bufferedFile);
        }catch (Exception ex){System.out.println("ERROR PARSING FILE");}
    }

    private Element getElementById(String id){
        if (id.equals("frb")) return Element.FIRE;
        if (id.equals("wtb")) return Element.WATER;
        if (id.equals("ntb")) return Element.NATURE;
        if (id.equals("etb")) return Element.EARTH;
        return null;
    }

    private void createObject(String object, float posx, float posy){
        Element element = getElementById(object);
        if (element != null){
            Bomb newBomb = new Bomb(element, world, posx, posy , GameVar.BOMB_WIDTH, GameVar.BOMB_HEIGHT);
            gameObjects.add(newBomb);
            bombsList.add(newBomb);
        } else return;
    }

    private BufferedReader getFileBuffer(FileHandle fh){
        return new BufferedReader(fh.reader());
    }

    private void moveSpritetBy(Sprite sprite, float x, float y){
        Vector2 position = sprite.getBodyTransform().getPosition();
        sprite.setBodyTransform(position.x+(x/PPM), position.y+(y/PPM), 0);
    }

    /********************* METHODS ***********************/
    public int getLevel(){return level;}
    public void playMusic(){
        background_music.setVolume(0.8f);
        background_music.play();
        background_music.setLooping(true);
    }

    public void stopMusic(){
        background_music.stop();
    }

    public void loadLevel(int i){
        clearAll();
        createWisp();
        /*--------- cache the file into a buffer -------*/
        BufferedReader bufferedFile = getFileBuffer(Gdx.files.internal("Levels/level"+Integer.toString(i)+"/content.txt"));
        /* ********* try to parse content of the file *******
        * the exemplar file would be:
        * Value : number
        * Background : image.png
        * GameObjects :
        * object object object ...
        */
        parseFile(bufferedFile);
        try{bufferedFile.close();}catch (Exception ex){}
    }

    public void remove(GameObject go){
        gameObjects.removeValue(go, true);
        bombsList.removeValue(go, true);
    }

    public boolean isExploding(){
        for (GameObject go : bombsList){
            Bomb bomb = (Bomb)go;
            if (bomb.status.equals(Status.EXPLOSION)) return true;
        }
        return false;
    }

    public void applyLevelEffect(){
        for (int i = 0; i < level; i++) increaseBombRow();
    }

    public void increaseDifficult(){level++;}

    public void increaseBombRow(){
        /*----------- move bombs ----------*/
        for (GameObject object : bombsList){
            moveSpritetBy((Sprite)object, 0, -(64+ GameVar.PADDING_Y));
        }

        /*---------- add bombs -----------*/
        float posx = GameVar.LEFT_EDGE;
        float posy = GameVar.TOP_EDGE;

        for (int i = 0; i < 10; i++){
            Bomb newbomb = new Bomb(Identity.randomElement(), world, posx, posy, GameVar.BOMB_WIDTH, GameVar.BOMB_HEIGHT);
            gameObjects.add(newbomb);
            bombsList.add(newbomb);
            posx += (64 + GameVar.PADDING_X);
        }
    }
}
