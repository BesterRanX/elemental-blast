package game.Main.Compositors;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.Viewport;

import game.BaseClasses.GUI.UIContainer;
import game.BaseClasses.GUI.buttons.ElementButton;
import game.BaseClasses.GUI.gameUI.DirectionController;
import game.BaseClasses.GUI.gameUI.ExitButton;
import game.BaseClasses.GUI.gameUI.Generator;
import game.BaseClasses.GUI.gameUI.ScoreLabel;
import game.BaseClasses.GUI.gameUI.TimeLabel;
import game.BaseClasses.Var.Element;
import game.BaseClasses.Var.GameVar;
import game.Main.GameApp;

/**
 * Created by root on 4/26/17.
 */

public class GUIManager {
    /************** COMPONENTS ***************/
    private GameApp gameApp;

    public UIContainer uiContainer;
    public DirectionController directionController;

    /*-------------- huds -------------*/
    public ScoreLabel scoreLabel;
    public Label timeLabel;
    private Table hudbackground;
    private ExitButton exitButton;

    /************* CONSTRUCTOR *************/
    public GUIManager(Viewport viewport, Batch batch, GameApp gapp){
        uiContainer = new UIContainer(viewport, batch);
        gameApp = gapp;
        loadGUI();
        loadHuds();
    }

    /**************** OPERATORS *************/
    private void loadGUI(){
        /*--------- create direction indicator --------*/
        directionController = new DirectionController(GameVar.D_CONTROLLER_X, GameVar.D_CONTROLLER_Y,
                GameVar.D_CONTROLLER_WIDTH, GameVar.D_CONTROLLER_HEIGHT, 0);// 0 is rotation value
        uiContainer.addActor(directionController);

        /*------------ create generator -----------------*/
        uiContainer.addActor(new Generator(GameVar.GENERATOR_X, GameVar.GENERATOR_Y,
                GameVar.GENERATOR_WIDTH, GameVar.GENERATOR_HEIGHT));

        /*------------ create element buttons -----------*/
        createElementButtons();
    }

    private void loadHuds(){
        hudbackground = new Table();
        hudbackground.setBackground(new TextureRegionDrawable(ResourceCache.framesList.get("component_hudbackground").first()));
        hudbackground.setBounds(-5, GameVar.VIRTUAL_SCREEN_HEIGHT - 70, GameVar.VIRTUAL_SCREEN_WIDTH + 10, 75 );

        /*------- initialise score label ---------*/
        LabelStyle labelStyle = new LabelStyle();
        labelStyle.font = new BitmapFont();
        labelStyle.fontColor = Color.ORANGE;
        scoreLabel = new ScoreLabel(labelStyle, "SCORE");
        scoreLabel.setFontScale(2.6f);
        scoreLabel.setBounds((GameVar.VIRTUAL_SCREEN_WIDTH - 160)/2, GameVar.VIRTUAL_SCREEN_HEIGHT - 40, 160, 20);

        /*--------- initialise time label --------------*/
        labelStyle = new LabelStyle();
        labelStyle.font = new BitmapFont();
        labelStyle.fontColor = Color.CYAN;
        timeLabel = new TimeLabel(labelStyle);
        timeLabel.setFontScale(2f);
        timeLabel.setBounds(10, GameVar.VIRTUAL_SCREEN_HEIGHT - 50, 120, 40);

        /*---------- initialise exit button -------------*/
        TextButtonStyle style = new TextButtonStyle();
        style.font = new BitmapFont();
        style.font.getData().setScale(3);

        exitButton = new ExitButton("Exit", style, gameApp);

        //exitButton.setBackground();
        exitButton.setBounds((GameVar.VIRTUAL_SCREEN_WIDTH - 160)/2 + 320, GameVar.VIRTUAL_SCREEN_HEIGHT - 50, 50, 50);

        /*---------- add initialised components ---------*/
        uiContainer.addActor(hudbackground);
        uiContainer.addActor(timeLabel);
        uiContainer.addActor(scoreLabel);
        uiContainer.addActor(exitButton);
    }

    private void createElementButtons(){
        /*-------------- create element buttons -----------*/
        ElementButton[] elementButtons = new ElementButton[]{
            new ElementButton(Element.FIRE, GameVar.FIREBUTTON_X, GameVar.FIREBUTTON_Y,
                 GameVar.ELEMENTBUTTON_RADIUS, GameVar.ELEMENTBUTTON_RADIUS),
            new ElementButton(Element.WATER, GameVar.WATERBUTTON_X, GameVar.WATERBUTTON_Y,
                 GameVar.ELEMENTBUTTON_RADIUS, GameVar.ELEMENTBUTTON_RADIUS),
            new ElementButton(Element.NATURE, GameVar.LEAFBUTTON_X, GameVar.LEAFBUTTON_Y,
                 GameVar.ELEMENTBUTTON_RADIUS, GameVar.ELEMENTBUTTON_RADIUS),
            new ElementButton(Element.EARTH, GameVar.EARTHBUTTON_X, GameVar.EARTHBUTTON_Y,
                 GameVar.ELEMENTBUTTON_RADIUS, GameVar.ELEMENTBUTTON_RADIUS),
        };

        /*--------- add created buttons to user interface container --------*/
        for (ElementButton eb : elementButtons) uiContainer.addActor(eb);
    }
}
