package game.Main.Screen;

import android.content.Context;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;

import game.BaseClasses.Actors.FrameObject;
import game.BaseClasses.GUI.UIContainer;
import game.BaseClasses.GUI.gameUI.ScoreLabel;
import game.BaseClasses.Var.GameVar;
import game.Main.Compositors.ResourceCache;
import game.Main.GameApp;
import gdx.android.AndroidLauncher;

/**
 * Created by root on 5/21/17.
 */

public class MainScreen implements Screen {
    /**************** ATTRIBUTES **************/
    /*------------ properties ------------*/
    private final int button_width = 350;
    private final int button_height = button_width/2;
    private final float title_width = GameVar.VIRTUAL_SCREEN_WIDTH;
    private final float title_height = title_width/2.6f;

    /*------------- components ----------*/
    private GameApp gameApp;
    private UIContainer uiContainer;
    private OrthographicCamera camera;
    private FitViewport viewport;

    /* ------------ ui elements ----------*/
    private FrameObject title;
    public TextButton play_button;
    public ScoreLabel scoreLabel;

    /*--------------- assets -------------*/

    /****************** CONSTRUCTOR *********************/
    public MainScreen(GameApp gameApp){
        this.gameApp = gameApp;
        camera = new OrthographicCamera();
        camera.setToOrtho(false);
        viewport = new FitViewport(GameVar.VIRTUAL_SCREEN_WIDTH, GameVar.VIRTUAL_SCREEN_HEIGHT);
        uiContainer = new UIContainer(viewport, gameApp.batch);

        initialise();
    }


    /******************* OPERATORS *******************/
    private void initialise(){
        //background = new Texture(Gdx.files.internal("GUI/backgrounds/main.png"));
        /*------------ initialise title -------*/
        TextureRegion title_texture = new TextureRegion(ResourceCache.framesList.get("mainui_title").first());
        title = new FrameObject(title_texture, (GameVar.VIRTUAL_SCREEN_WIDTH - title_width)/2,
                GameVar.VIRTUAL_SCREEN_HEIGHT * 0.77f, title_width, title_height);

        /*------------ button style ----------*/
        TextButtonStyle style = new TextButtonStyle();
        style.up = new TextureRegionDrawable(ResourceCache.framesList.get("mainui_button").first());
        style.font = new BitmapFont();
        style.font.getData().setScale(6f);
        style.font.getData().lineHeight = 2;

        /*------------- play button -----------*/
        play_button = new TextButton("Play", style);
        play_button.setBounds((GameVar.VIRTUAL_SCREEN_WIDTH - button_width)/2,
                (GameVar.VIRTUAL_SCREEN_HEIGHT -button_height)/2 + 50, button_width, button_height);
        play_button.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                gameApp.adHandler.toggleAds();
                gameApp.setScreen(gameApp.getPlayScreenInstance());
            }
        });


        /*------------- add max score ----------*/
        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = new BitmapFont();
        labelStyle.fontColor = Color.SCARLET;
        //labelStyle.background = new TextureRegionDrawable(ResourceCache.framesList.get("mainui_button").first());

        scoreLabel = new ScoreLabel(labelStyle, "RECORD");
        scoreLabel.setFontScale(4f);
        scoreLabel.setScore(scoreLabel.readScore());
        scoreLabel.setBounds(play_button.getX(), GameVar.VIRTUAL_SCREEN_HEIGHT - play_button.getY()-play_button.getHeight(), 160, 20);

        /*------------- finally  -----------*/
        uiContainer.addActor(play_button);
        uiContainer.addFrame(title);
        uiContainer.addActor(scoreLabel);
        Gdx.input.setInputProcessor(uiContainer);
    }


    /****************** METHODS ********************/
    public void draw(){
        /*-------- clear -----*/
        Gdx.gl20.glClearColor(0,0.9f,0.7f,1f);
        Gdx.gl20.glClear(Gdx.gl20.GL_COLOR_BUFFER_BIT);

        /*------- draw -----*/
        uiContainer.draw();
    }

    public void update(final float delta){
        camera.update();
        uiContainer.act(delta);
    }

    @Override
    public void render(final float delta) {
        if (delta >= GameVar.FRAME_TICK) update(delta);
        draw();
    }

    @Override
    public void resize(int width, int height) {}
    @Override
    public void pause() {}

    @Override
    public void resume() {
        scoreLabel.setScore(scoreLabel.readScore());
    }

    @Override
    public void hide() {}
    @Override
    public void show() {}
    @Override
    public void dispose() {}
}
