package game.Main.Screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import game.BaseClasses.Actors.GameObject;
import game.BaseClasses.Var.GameVar;
import game.Controls.CollisionListener;
import game.Main.Compositors.GUIManager;
import game.Main.Compositors.GameManager;
import game.Main.GameApp;

/**
 * Created by root on 4/15/17.
 */

public class PlayScreen implements Screen{
    /************ ATTRIBUTES *************/
    private GameApp gameApp;//reference to MainApp
    public static GameManager gameManager;
    public static GUIManager guiManager;

    //------- camera
    public static GameCamera gameCamera;
    private Viewport guiVieport;

    //------- physic engine
    private World physicWorld;


    /*************** CONSTRUCTOR **************/
    public PlayScreen(final GameApp game){
        //-------! initialise components !------
        gameApp = game;//get reference

        //------ camera
        gameCamera = new GameCamera();
        //gameCamera.setToOrtho(false, GameVar.VIRTUAL_SCREEN_WIDTH, GameVar.VIRTUAL_SCREEN_HEIGHT);
        guiVieport = new FitViewport(GameVar.VIRTUAL_SCREEN_WIDTH, GameVar.VIRTUAL_SCREEN_HEIGHT, gameCamera);

        //------- setup game variables
        GameVar.setupGameVariables(gameCamera);

        //-------! physic engine
        physicWorld = new World(new Vector2(0, 0), true);
        physicWorld.setContactListener(new CollisionListener());

        //-------! load level
        gameManager = new GameManager(physicWorld);
        gameManager.loadLevel(1);

        //-------! setup use interface
        guiManager = new GUIManager(guiVieport, gameApp.batch, gameApp);
        Gdx.input.setInputProcessor(guiManager.uiContainer);

        //-------! setup music
        gameManager.playMusic();
        gameManager.background_music.setVolume(GameApp.music_volume);

        //-------! setup background

    }


    /**************** METHODS **************/
    public void update(final float delta){
        /*---- update gameobjects----*/
        for (GameObject go : gameManager.gameObjects){
             go.update(delta);
        }

        guiManager.uiContainer.act(delta);
    }

    public void draw(final float delta){
        /*-------- clear -----*/
        Gdx.gl20.glClearColor(0,0,0,1);
        Gdx.gl20.glClear(Gdx.gl20.GL_COLOR_BUFFER_BIT);

        /*---- apply camera properties to the batch -----*/
        gameApp.batch.setProjectionMatrix(gameCamera.combined);
        gameCamera.update(delta);

        /******* begin to draw in batch *******/


        /* ----- draw game objects ------*/
        gameApp.batch.begin();
        /* -- draw background --*/
        gameApp.batch.draw(gameManager.background, 0, 0, gameCamera.viewportWidth, gameCamera.viewportHeight);
        /* -- draw gameobjects -- */
        for (GameObject go : gameManager.gameObjects){
            go.draw(gameApp.batch, delta);
        }

        gameApp.batch.end();
        /* ----- draw user interface -----*/
        guiManager.uiContainer.draw();
    }

    @Override
    public void render(final float delta) {
        /* update 30 times each second (30 FPS) */
        if (delta >= GameVar.FRAME_TICK) update(delta);
        /* detect physic collision each 6 velocity iteration
        *  detect physic collision each 3 position iteration
        *  */
        physicWorld.step(delta, 6, 3);

        /* draw everything */
        draw(delta);
    }

    @Override
    public void dispose() {
        physicWorld.dispose();
        gameManager.background_music.dispose();
        guiManager.uiContainer.dispose();
    }

    @Override
    public void show() {}
    @Override
    public void resize(int width, int height) {
        guiVieport.update(width, height);
    }
    @Override
    public void pause() {}
    @Override
    public void resume() {}
    @Override
    public void hide() {}

}
