package game.Main.Screen;

import com.badlogic.gdx.graphics.OrthographicCamera;

import game.BaseClasses.ToolKits.Shaker;
import game.BaseClasses.Var.Status;

/**
 * Created by root on 4/26/17.
 */

public class GameCamera extends OrthographicCamera {
    /************ ATTRIBUTES ************/
    public Status status;
    private Shaker shaker;


    /************ CONSTRUCTOR ************/
    public GameCamera(){
        super();

        status = Status.STANDBY;
    }

    public GameCamera(int width, int height){
        super(width, height);
        shaker = new Shaker();

        status = Status.STANDBY;
    }


    /************* OPERATORS *************/
    private void applyShaking(float delta){
        shaker.shake(this, delta);
        if (shaker.isFinished()) {
            status = Status.STANDBY;
            this.setToOrtho(false, this.viewportWidth, this.viewportHeight);
        }
    }


    /************** METHODS ******************/
    public void shake(float duration, float frequency){
        if (status.equals(Status.SHAKE) && !shaker.isFinished()){
            shaker.extendShakeDuration(duration/2);
        } else {
            status = Status.SHAKE;
            shaker = new Shaker(-1, 1, duration, frequency);
        }
    }

    public void update(float delta){
        super.update();
        if (status.equals(Status.SHAKE)) {
            applyShaking(delta);
        }
    }
}
