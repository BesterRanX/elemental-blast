package game.main.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;

import game.base.actors.FrameObject;
import game.base.gameui.UIContainer;
import game.base.toolkits.ScoreManager;
import game.variables.GlobalVariable;
import game.main.compositors.ResourceCache;
import game.main.GameApp;
import gdx.android.googleaccount.GoogleAccountHandler;

import static game.base.toolkits.PackLoader.separator;
import static game.variables.GlobalVariable.LOGINBUTTON_HEIGHT;
import static game.variables.GlobalVariable.LOGINBUTTON_WIDTH;
import static game.variables.GlobalVariable.MUTATIONBUTTON_HEIGHT;
import static game.variables.GlobalVariable.MUTATIONBUTTON_WIDTH;
import static game.variables.GlobalVariable.MUTATION_ICON_HEIGHT;
import static game.variables.GlobalVariable.MUTATION_ICON_WIDTH;
import static game.variables.GlobalVariable.PLAYBUTTON_HEIGHT;
import static game.variables.GlobalVariable.PLAYBUTTON_WIDTH;
import static game.variables.GlobalVariable.SCOREBOARD_HEIGHT;
import static game.variables.GlobalVariable.SCOREBOARD_WIDTH;

/**
 * Created by root on 5/21/17.
 */

public class MainScreen implements Screen{
    /************* ATTRIBUTES **************/
    /*----------- properties ------------*/
    private final float title_width = GlobalVariable.VIRTUAL_SCREEN_WIDTH;
    private final float title_height = title_width/2.6f;

    /*------------- hud ----------------*/
    private GameApp gameApp;
    private UIContainer uiContainer;
    private OrthographicCamera camera;
    private FitViewport viewport;
    private ScoreManager scoreManager;

    private Music main_music;

    /* ---------- ui elements ----------*/
    protected FrameObject title;
    protected FrameObject mutation_icon;
    protected Texture background;
    protected TextButton play_button;
    protected TextButton login_button;
    protected TextButton scoreBoard;
    protected TextButton mutation_button;

    /*---------- google account -------*/
    protected GoogleAccountHandler accountHandler;
    /******************* CONSTRUCTOR *********************/
    public MainScreen(GameApp gameapp, GoogleAccountHandler accHandler){
        gameApp = gameapp;
        accountHandler = accHandler;
        scoreManager = new ScoreManager();

        camera = new OrthographicCamera();
        camera.setToOrtho(false);
        viewport = new FitViewport(GlobalVariable.VIRTUAL_SCREEN_WIDTH, GlobalVariable.VIRTUAL_SCREEN_HEIGHT);
        uiContainer = new UIContainer(viewport, gameApp.batch);

        initialise_Components();
        // initialise_GoogleAccount();
    }

    /******************* OPERATORS **********************/
    /*
    private void initialise_GoogleAccount() {
        accountHandler.addGoogleSignListener(new GoogleSignListener() {
            @Override
            public void onSignIn() {
                Log.d("showing ui", "showing");
                showUI();
            }

            @Override
            public void onSignOut() {
                Log.d("hideing ui","hiding");
                hideUI();
            }
        });
        // check signed account
        // AndroidLauncher.googleAccount = accountHandler.getSignedAccount();
    }
    */
    private void initialise_Components(){
        background = new Texture(Gdx.files.internal("userinterface/backgrounds/2.png"));
        main_music = Gdx.audio.newMusic(Gdx.files.internal("sounds/background_music/main.mp3"));
        main_music.setVolume(GlobalVariable.MUSIC_VOLUME);
        main_music.setLooping(true);

        /*------------ initialise_Components title -------*/
        TextureRegion title_texture = new TextureRegion(ResourceCache.framesList.get("mainui"+separator+"title").first());
        title = new FrameObject(title_texture, (GlobalVariable.VIRTUAL_SCREEN_WIDTH - title_width)/2,
                GlobalVariable.VIRTUAL_SCREEN_HEIGHT * 0.77f, title_width, title_height);

        /*------------- login button ----------*/
        TextButtonStyle loginbutton_style = new TextButtonStyle();

             /*---- login button color ---*/
        Pixmap loginbtn_background_color = new Pixmap(LOGINBUTTON_WIDTH, LOGINBUTTON_HEIGHT, Pixmap.Format.RGB888);
        loginbtn_background_color.setColor(Color.YELLOW);
        loginbtn_background_color.fill();
        loginbutton_style.up = new TextureRegionDrawable(ResourceCache.framesList.get("mainui"+separator+"button_redskin").first());
        loginbutton_style.font = new BitmapFont();
        loginbutton_style.font.getData().setScale(5.5f);
        loginbutton_style.font.getData().lineHeight = 4;

        login_button = new TextButton("Login", loginbutton_style);
        login_button.setBounds(
                (GlobalVariable.VIRTUAL_SCREEN_WIDTH - LOGINBUTTON_WIDTH)/2,
                (GlobalVariable.VIRTUAL_SCREEN_HEIGHT - LOGINBUTTON_HEIGHT)/2 + 160,
                LOGINBUTTON_WIDTH,
                LOGINBUTTON_HEIGHT
        );

        login_button.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                accountHandler.SignIn();
                // AndroidLauncher.googleAccount = accountHandler.getSignedAccount();
            }
        });

        /*------------- play button -----------*/
        TextButtonStyle playbutton_style = new TextButtonStyle();
        playbutton_style.up = new TextureRegionDrawable(ResourceCache.framesList.get("mainui"+separator+"button_blueskin").first());
        playbutton_style.font = new BitmapFont();
        playbutton_style.font.getData().setScale(5f);
        playbutton_style.font.getData().lineHeight = 2;

        play_button = new TextButton("Play", playbutton_style);
        play_button.setBounds(
                (GlobalVariable.VIRTUAL_SCREEN_WIDTH - PLAYBUTTON_WIDTH)/2,
                (GlobalVariable.VIRTUAL_SCREEN_HEIGHT - PLAYBUTTON_HEIGHT)/2 + 160,
                PLAYBUTTON_WIDTH,
                PLAYBUTTON_HEIGHT
        );

        play_button.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                gameApp.adHandler.toggleAds();
                gameApp.setScreen(gameApp.newPlayScreen());
                dispose();
            }
        });

        /*------------- mutation button ------------*/
        TextButtonStyle mutationbutton_style = new TextButtonStyle();
        mutationbutton_style.up = new TextureRegionDrawable(ResourceCache.framesList.get("mainui"+separator+"button_blueskin").first());
        mutationbutton_style.font = new BitmapFont();
        mutationbutton_style.fontColor = Color.GREEN;
        mutationbutton_style.font.getData().setScale(4f);
        mutationbutton_style.font.getData().lineHeight = 2;

        mutation_button = new TextButton("   Mutation", mutationbutton_style);
        mutation_button.setBounds(
                play_button.getX(),
                GlobalVariable.VIRTUAL_SCREEN_HEIGHT - play_button.getY() - 40,
                MUTATIONBUTTON_WIDTH,
                MUTATIONBUTTON_HEIGHT
        );

        mutation_button.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                // Log.d("account info:", AndroidLauncher.googleAccount.getEmail());
            }
        });

        TextureRegion mutation_icon_texture = new TextureRegion(ResourceCache.framesList.get("mainui"+separator+"mutation_icon").first());
        mutation_icon = new FrameObject(
                mutation_icon_texture,
                mutation_button.getX() - 10,
                mutation_button.getY() + mutation_button.getHeight()/4,
                MUTATION_ICON_WIDTH,
                MUTATION_ICON_HEIGHT
        );

        /*------------- scoreboard ----------*/
        TextButtonStyle scoreBoardStyle = new TextButtonStyle();
        scoreBoardStyle.up = new TextureRegionDrawable(ResourceCache.framesList.get("hud"+separator+"scorelabel").first());
        scoreBoardStyle.font = new BitmapFont();
        scoreBoardStyle.fontColor = Color.GOLD;
        scoreBoardStyle.font.getData().setScale(3f);
        scoreBoardStyle.font.getData().lineHeight = 1;

        scoreBoard = new TextButton(Integer.toString(ScoreManager.readScore()), scoreBoardStyle);
        scoreBoard.setBounds(
                mutation_button.getX(),
                GlobalVariable.VIRTUAL_SCREEN_HEIGHT - play_button.getY() - play_button.getHeight() - 20,
                SCOREBOARD_WIDTH,
                SCOREBOARD_HEIGHT
        );

        /*------------- finally  -----------*/
        // uiContainer.addFrame(mainBackground);
        uiContainer.addFrame(title);
        // uiContainer.addActor(login_button);
        uiContainer.addActor(play_button);
        // uiContainer.addActor(mutation_button);
        // uiContainer.addFrame(mutation_icon);
        uiContainer.addActor(scoreBoard);

        // showUI();
        Gdx.input.setInputProcessor(uiContainer);
        main_music.play();
    }

    /*
    public void hideUI(){
        login_button.setVisible(true);
        play_button.setVisible(false);
        scoreBoard.setVisible(false);
        mutation_button.setVisible(false);
        mutation_icon.setVisibile(false);
    }

    public void showUI(){
        // login_button.setVisible(false);
        play_button.setVisible(true);
        scoreBoard.setVisible(true);

        mutation_button.setVisible(true);
        mutation_icon.setVisibile(true);

    }
    */

    /****************** METHODS ********************/
    public void draw(){
        /*-------- clear -----*/
        Gdx.gl20.glClearColor(0,0,0,1);
        Gdx.gl20.glClear(Gdx.gl20.GL_COLOR_BUFFER_BIT);

        /*------- draw -----*/
        gameApp.batch.begin();
        gameApp.batch.draw(background, 0 ,0 , background.getWidth(), background.getHeight());
        gameApp.batch.end();
        uiContainer.draw();
    }

    public void update(final float delta){
        camera.update();
        uiContainer.act(delta);
    }

    @Override
    public void render(final float delta) {
        if (delta >= GlobalVariable.FRAME_TICK) update(delta);
        draw();
    }

    @Override
    public void resize(int width, int height) {}
    @Override
    public void pause() {}

    @Override
    public void resume() {
        scoreBoard.setText(Integer.toString(scoreManager.readScore()));
    }

    @Override
    public void hide() {}
    @Override
    public void show() {}
    @Override
    public void dispose() {
        uiContainer.dispose();
        main_music.dispose();
    }
}
