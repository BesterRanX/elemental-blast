package game.main.compositors;

import android.util.Log;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import java.io.BufferedReader;
import java.io.IOException;

import game.base.actors.Bomb;
import game.base.actors.FrameObject;
import game.base.actors.GameObject;
import game.base.actors.Sprite;
import game.base.actors.Wisp;
import game.base.toolkits.ScoreManager;
import game.main.GameApp;
import game.variables.Element;
import game.variables.GlobalVariable;
import game.variables.Identity;
import game.variables.Status;

import static game.base.toolkits.PackLoader.separator;
import static game.variables.GlobalVariable.DEADLINE_HEIGHT;
import static game.variables.GlobalVariable.PPM;
import static game.variables.GlobalVariable.Y_DEADLINE;

/**
 * Created by root on 4/18/17.
 */

public class GameManager {
    /*********** ATTRIBUTES ***************/
    private GameApp gameApp;

    /*--------- core variables --------- */
    private World world;//reference to the world
    private int level = 0;
    public Texture background ;
    public boolean isPause = false;
    private boolean isPlaying = false;

    /* ------- bombs ---------- */
    public Array<GameObject> gameObjects;
    public Array<Bomb> bombsList;
    public Array<Bomb> lastRow;

    /* ------- wisp ------------*/
    public static Wisp wisp;

    /* -------- music ---------- */
    public Music background_music;

    /* -------- deadline -------*/

    /************* CONSTRUCTOR ***************/
    public GameManager(World world, GameApp gameApp){
        //get reference
        this.world = world;
        this.gameApp = gameApp;

        bombsList = new Array<>();
        gameObjects = new Array<GameObject>();
        lastRow = new Array<>();

        background_music = Gdx.audio.newMusic(Gdx.files.internal("sounds/background_music/game.mp3"));
    }


    /************* OPERATORS *****************/
    private void createWisp(){
        wisp = new Wisp(world, GlobalVariable.START_X, GlobalVariable.START_Y, GlobalVariable.WISP_WIDTH, GlobalVariable.WISP_HEIGHT);
        gameObjects.add(wisp);
    }

    private void clearAll(){
        for(GameObject go : gameObjects){
            go.dispose();
        }
        gameObjects.clear();
    }

    private void parseGameObjects(BufferedReader buffer) throws IOException{
        String line;
        float posx = GlobalVariable.LEFT_EDGE;
        float posy = GlobalVariable.TOP_EDGE;

        /*------- read line by line --------*/
        while (!(line = buffer.readLine()).isEmpty()){
            String[] lineWords = line.split(" ");

            /*--------- parse each word --------*/
            for (int i = 0; i < lineWords.length; i++){
                createObjectById(lineWords[i], posx, posy);
                posx += (64 + GlobalVariable.PADDING_X);
            }
            posx = GlobalVariable.LEFT_EDGE;
            posy -= (64 + GlobalVariable.PADDING_Y);
        }
    }

    private void createObjectById(String id, float posx, float posy){
        if (id.equals("frb") || id.equals("wtb") || id.equals("ntb") || id.equals("etb"))
            createObject(id, posx, posy);
    }

    private void parseProperties(BufferedReader bufferedfile) throws IOException{
        String line;
        boolean gameobjects = false;
        while(!gameobjects && !(line = bufferedfile.readLine()).isEmpty()){
            /*------- temp cache ------*/
            String[] word = line.split(" ");
            for (int i = 0; i < word.length; i++){
                if (word[i].equals(":")){
                    if (word[i-1].equals("Index")) level = Integer.parseInt(word[i+1]);
                    if (word[i-1].equals("Background")) background = new Texture("userinterface/backgrounds/" + word[i+1]);
                    if (word[i-1].equals("GameObjects")) gameobjects = true;
                }
            }
        }
    }

    private void parseFile(BufferedReader bufferedFile){
        try{
            parseProperties(bufferedFile);
            parseGameObjects(bufferedFile);
        }catch (Exception ex){System.out.println("ERROR PARSING FILE");}
    }

    private Element getElementById(String id){
        if (id.equals("frb")) return Element.FIRE;
        if (id.equals("wtb")) return Element.WATER;
        if (id.equals("ntb")) return Element.GRASS;
        if (id.equals("etb")) return Element.EARTH;
        return null;
    }

    private void createObject(String object, float posx, float posy){
        Element element = getElementById(object);
        if (element != null){
            Bomb newBomb = new Bomb(element, world, posx, posy , GlobalVariable.BOMB_WIDTH, GlobalVariable.BOMB_HEIGHT);
            gameObjects.add(newBomb);
            bombsList.add(newBomb);
        } else return;
    }

    private BufferedReader getFileBuffer(FileHandle fh){
        return new BufferedReader(fh.reader());
    }

    private void moveSpritetBy(Sprite sprite, float x, float y){
        Vector2 position = sprite.getBodyTransform().getPosition();
        sprite.setBodyTransform(position.x+(x/PPM), position.y+(y/PPM), 0);
    }

    /********************* METHODS ***********************/
    public void endGame(){
        isPause = true; // pausa il gioco
        stopMusic();

        // memorizza il score
        ScoreManager.checkRecord(GameApp.playScreen.guiManager.scoreLabel.getScore());

        gameApp.setScreen(gameApp.newMainScreen());
        dispose();
        GameApp.playScreen.dispose();
        gameApp.adHandler.toggleAds(); //toggle on
    }

    public void setPlaying(boolean s){
        isPlaying = s;
    }

    public boolean isPlaying(){
        return isPlaying;
    }

    public int getLevel(){return level;}
    public void playMusic(){
        background_music.setVolume(0.8f);
        background_music.play();
        background_music.setLooping(true);
    }

    public void stopMusic(){
        background_music.stop();
    }

    public void loadLevel(int i){
        clearAll();
        createWisp();
        /*--------- cache the file into a buffer -------*/
        BufferedReader bufferedFile = getFileBuffer(Gdx.files.internal("level/content.txt"));
        /* ********* try to parse content of the file *******
        * the exemplar file would be:
        * Value : number
        * Background : image.png
        * GameObjects :
        * object object object ...
        */
        parseFile(bufferedFile);
        try{bufferedFile.close();}catch (Exception ex){}
    }

    public void remove(GameObject go){
        gameObjects.removeValue(go, true);
    }

    public void remove(Bomb bomb){
        gameObjects.removeValue((GameObject)bomb, true);
        bombsList.removeValue(bomb, true);
    }

    public boolean isExploding(){
        for (GameObject go : bombsList){
            Bomb bomb = (Bomb)go;
            if (bomb.status.equals(Status.EXPLOSION)) return true;
        }
        return false;
    }

    public Array<Bomb> getLastRow(){return lastRow;}

    public float getY_lastrow(){
        Log.i("last row y:", Float.toString(lastRow.get(0).position.y));
        return lastRow.get(0).position.y;
    }

    public void applyLevelEffect(){
        for(int i =0; i < level; i++){
            increaseBombRow(true);
        }
    }

    public void increaseDifficult(){
        level++;
    }

    public void increaseBombRow(boolean movedown){
        if(movedown){
            /*------- move bombs ----------*/
            for (Bomb bomb : bombsList){
                moveSpritetBy(bomb, 0, -(64+ GlobalVariable.PADDING_Y));
                // Log.i("bomb position: ", Float.toString(bomb.position.y));
                if(bomb.position.y - GlobalVariable.BOMB_HEIGHT <= Y_DEADLINE){
                    endGame();
                    break;
                }
            }
        }

        /*---------- add bombs -----------*/
        float posx = GlobalVariable.LEFT_EDGE;
        float posy = GlobalVariable.TOP_EDGE;

        for (int i = 0; i < 10; i++){
            Bomb newbomb = new Bomb(Identity.randomElement(), world, posx, posy, GlobalVariable.BOMB_WIDTH, GlobalVariable.BOMB_HEIGHT);
            gameObjects.add(newbomb);
            bombsList.add(newbomb);
            lastRow.add(newbomb);
            posx += (64 + GlobalVariable.PADDING_X);
        }
    }

    public void dispose(){
        wisp.dispose();
        bombsList.clear();
        background_music.dispose();
        isPlaying = false;
    }
}
