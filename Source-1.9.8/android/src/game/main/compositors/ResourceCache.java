package game.main.compositors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;

import game.base.toolkits.SoundLoader;
import game.base.toolkits.PackLoader;

/**
 * Created by root on 4/15/17.
 */

public class ResourceCache {
    /********* cached resources *********/
    public static HashMap<String, Array<AtlasRegion>> framesList;
    public static HashMap<String, Sound> soundsList;

    /*********** attributes ************/
    private SoundLoader soundLoader;
    public static PackLoader packLoader;

    /*********** CONSTRUCTOR **********/
    public ResourceCache(){
        soundLoader = new SoundLoader();
        packLoader = new PackLoader("-");

        framesList = new HashMap<String, Array<AtlasRegion>>();
        soundsList = new HashMap<String, Sound>();

        loadFrames();
        loadSounds();

        packLoader.clear();
        soundLoader.clear();
    }

    /************* OPERATORS **********/
    private void loadFrames(){
        packLoader.loadPacks(Gdx.files.internal("packs.xml"));
        framesList.putAll(packLoader.getFrameList());
    }

    private void loadSounds(){
        soundLoader.loadResources(Gdx.files.internal("soundlist"));
        soundsList.putAll(soundLoader.getSounds());
    }
}
