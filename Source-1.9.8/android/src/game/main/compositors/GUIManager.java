package game.main.compositors;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.Viewport;

import game.base.gameui.UIContainer;
import game.base.gameui.buttons.ElementButton;
import game.base.gameui.hud.DirectionController;
import game.base.gameui.hud.HomeButton;
import game.base.gameui.hud.Generator;
import game.base.gameui.hud.ScoreLabel;
import game.base.gameui.hud.TimeLabel;
import game.variables.Element;
import game.variables.GlobalVariable;
import game.main.GameApp;

import static game.base.toolkits.PackLoader.separator;

/**
 * Created by root on 4/26/17.
 */

public class GUIManager {
    /************** COMPONENTS ***************/
    private GameApp gameApp;

    public UIContainer uiContainer;
    public DirectionController directionController;

    /*--------------- huds -----------------*/
    public ScoreLabel scoreLabel;
    public Label timeLabel;
    private Table hudbackground;
    private HomeButton homebutton;

    /************* CONSTRUCTOR ****************/
    public GUIManager(Viewport viewport, Batch batch, GameApp gapp){
        uiContainer = new UIContainer(viewport, batch);
        gameApp = gapp;
        loadGUI();
        loadHuds();
    }

    /**************** OPERATORS ****************/
    private void loadGUI(){
        /*---------- create direction indicator --------*/
        directionController = new DirectionController(GlobalVariable.D_CONTROLLER_X, GlobalVariable.D_CONTROLLER_Y,
                GlobalVariable.D_CONTROLLER_WIDTH, GlobalVariable.D_CONTROLLER_HEIGHT, 0);// 0 is rotation value
        uiContainer.addActor(directionController);

        /*------------ create generator ---------------*/
        uiContainer.addActor(new Generator(GlobalVariable.GENERATOR_X, GlobalVariable.GENERATOR_Y,
                GlobalVariable.GENERATOR_WIDTH, GlobalVariable.GENERATOR_HEIGHT));

        /*------------ create element buttons ---------*/
        createElementButtons();
    }

    private void loadHuds(){
        hudbackground = new Table();
        hudbackground.setBackground(new TextureRegionDrawable(ResourceCache.framesList.get("hud"+separator+"hudbackground").first()));
        hudbackground.setBounds(-5, GlobalVariable.VIRTUAL_SCREEN_HEIGHT - 70, GlobalVariable.VIRTUAL_SCREEN_WIDTH + 10, 75 );

        /*--------- initialise score label ---------*/
        LabelStyle labelStyle = new LabelStyle();
        labelStyle.font = new BitmapFont();
        labelStyle.fontColor = Color.GOLD;
        labelStyle.background = new TextureRegionDrawable(ResourceCache.framesList.get("hud"+separator+"scorelabel").first());
        scoreLabel = new ScoreLabel(labelStyle);
        scoreLabel.setFontScale(2);
        scoreLabel.setAlignment(Align.center);
        scoreLabel.setBounds((GlobalVariable.VIRTUAL_SCREEN_WIDTH - 160)/2, GlobalVariable.VIRTUAL_SCREEN_HEIGHT - 60, 180, 55);

        /*--------- initialise time label --------*/
        labelStyle = new LabelStyle();
        labelStyle.font = new BitmapFont();
        labelStyle.fontColor = Color.ORANGE;
        labelStyle.background = new TextureRegionDrawable(ResourceCache.framesList.get("hud"+separator+"timelabel").first());
        timeLabel = new TimeLabel(labelStyle);
        timeLabel.setFontScale(2);
        timeLabel.setAlignment(Align.center);
        timeLabel.setBounds(10, GlobalVariable.VIRTUAL_SCREEN_HEIGHT - 60, 180, 55);

        /*---------- initialise exit button -------*/
        TextButtonStyle style = new TextButtonStyle();
        style.font = new BitmapFont();
        style.font.getData().setScale(3);
        style.up = new TextureRegionDrawable(ResourceCache.framesList.get("mainui"+separator+"button_home").first());
        homebutton = new HomeButton(style, gameApp);
        homebutton.setBounds((GlobalVariable.VIRTUAL_SCREEN_WIDTH - 160)/2 + 320, GlobalVariable.VIRTUAL_SCREEN_HEIGHT - 65, 63, 63);

        /*---------- add initialised hud ---------*/
        uiContainer.addActor(hudbackground);
        uiContainer.addActor(timeLabel);
        uiContainer.addActor(scoreLabel);
        uiContainer.addActor(homebutton);
    }

    private void createElementButtons(){
        /*------------------ create element buttons ----------------*/
        ElementButton[] elementButtons = new ElementButton[]{
            new ElementButton(Element.FIRE, GlobalVariable.FIREBUTTON_X, GlobalVariable.FIREBUTTON_Y,
                 GlobalVariable.ELEMENTBUTTON_RADIUS, GlobalVariable.ELEMENTBUTTON_RADIUS),
            new ElementButton(Element.WATER, GlobalVariable.WATERBUTTON_X, GlobalVariable.WATERBUTTON_Y,
                 GlobalVariable.ELEMENTBUTTON_RADIUS, GlobalVariable.ELEMENTBUTTON_RADIUS),
            new ElementButton(Element.GRASS, GlobalVariable.LEAFBUTTON_X, GlobalVariable.LEAFBUTTON_Y,
                 GlobalVariable.ELEMENTBUTTON_RADIUS, GlobalVariable.ELEMENTBUTTON_RADIUS),
            new ElementButton(Element.EARTH, GlobalVariable.EARTHBUTTON_X, GlobalVariable.EARTHBUTTON_Y,
                 GlobalVariable.ELEMENTBUTTON_RADIUS, GlobalVariable.ELEMENTBUTTON_RADIUS),
        };

        /*--------- add created buttons to user interface container --------*/
        for (ElementButton eb : elementButtons) uiContainer.addActor(eb);
    }

    public void dispose() {
        directionController.clear();
        scoreLabel.clear();
        timeLabel.clear();
        hudbackground.clear();
        homebutton.clear();
        uiContainer.dispose();
    }
}
