package game.main;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.main.compositors.ResourceCache;
import game.main.screen.MainScreen;
import game.main.screen.PlayScreen;
import gdx.android.admob.AdHandler;
import gdx.android.googleaccount.GoogleAccountHandler;

public class GameApp extends Game implements ApplicationListener {
	/*-------- application context --------*/
	private static GoogleAccountHandler googleAccountHandler;

    public AdHandler adHandler;

	/********* static attributes **********/

	/****** screens ******/
    private static MainScreen mainScreen;
	public static PlayScreen playScreen;

	/***** resources *****/
    public ResourceCache RC;
	public SpriteBatch batch;

	/************** COSTRUCTOR *************/
    public GameApp(AdHandler ad, GoogleAccountHandler gah){
    	googleAccountHandler = gah;
    	adHandler = ad;
	}

	@Override
	public void create () {
		batch = new SpriteBatch();
		RC = new ResourceCache();

		mainScreen = new MainScreen(this, googleAccountHandler);
		setScreen(mainScreen);
	}

	public MainScreen newMainScreen(){
		return new MainScreen(this, googleAccountHandler);
	}

	public PlayScreen newPlayScreen(){
		playScreen = new PlayScreen(this);
		return playScreen;
	}

	@Override
	public void render () {
		super.render();
	}
	@Override
	public void dispose () {
		batch.dispose();
	}
}
