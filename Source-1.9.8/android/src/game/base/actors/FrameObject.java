package game.base.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by root on 5/19/17.
 */

public class FrameObject{
    /************* ATTRIBUTES **************/
    public Vector2 position;
    protected TextureRegion currentFrame;
    protected float width, height;
    protected boolean isVisible = true;

    /************* CONSTRUCTOR *************/
    public FrameObject(float _posx, float _posy, float _width, float _height){
        position = new Vector2(_posx, _posy);
        width(_width).height(_height);
    }

    public FrameObject(TextureRegion frame, float _posx, float _posy, float _width, float _height){
        position = new Vector2(_posx, _posy);
        width(_width).height(_height);
        setTexture(frame);
    }

    public FrameObject(TextureRegion frame, Vector2 _position, float _width, float _height){
        setTexture(frame);
        width(_width).height(_height);
        position = new Vector2(_position);
    }

    public FrameObject(Texture texture, float posx, float posy, float width, float height){
        setTexture(texture);
        width(width).height(height);
        position = new Vector2(posx, posy);
    }

    /************* OPERATOR **************/
    public FrameObject setTexture(TextureRegion textureRegion){
        currentFrame = textureRegion;
        return this;
    }

    public FrameObject setTexture(Texture texture){
        currentFrame.setTexture(texture);
        return this;
    }

    public TextureRegion getCurrentFrame(){
        return currentFrame;
    }

    public float getWidth(){
        return width;
    }

    public float getHeight(){
        return height;
    }

    /************* METHODS **************/

    public FrameObject width(float w){
        this.width = w;
        return this;
    }

    public FrameObject height(float h){
        this.height = h;
        return this;
    }

    public void setVisibile(boolean v){
        isVisible = v;
    }

    public boolean isVisible() {
        return isVisible;
    }
}
