package game.base.actors;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Transform;
import com.badlogic.gdx.physics.box2d.World;

import game.variables.Identity;
import game.variables.Status;

import static game.variables.GlobalVariable.PPM;

/**
 * Created by root on 4/16/17.
 */

public abstract class Sprite extends FrameObject implements GameObject {

    /*************** ATTRIBUTES ***************/
    protected World world;
    protected Body body;
    protected Fixture fixture;
    public Identity identity;
    public Status status;

    /*************** CONSTRUCTOR **************/
    public Sprite(Identity identity, World world, float posx, float posy, float width, float height){
        super(posx, posy, width, height);
        this.world = world;
        this.identity = identity;
        this.status = Status.STANDBY;
    }

    /************* SETTING/GETTING ***********/
    public void setBodyType(BodyDef.BodyType btype){body.setType(btype);}
    public void setBodyTransform(float x, float y, float a){body.setTransform(x, y, a);}
    public Transform getBodyTransform(){return body.getTransform();}

    /**************** OPERATORS **************/
    protected void createBody(BodyDef.BodyType btype){
        BodyDef bdef = new BodyDef();
        /*---- convert body to pixel per meters ---*/
        bdef.position.set(position.x/PPM, position.y/PPM);
        bdef.type = btype;
        body = world.createBody(bdef);
        body.setAwake(true);
    }

    protected void createFixture(FixtureDef fdef){
        fixture = body.createFixture(fdef);
        fixture.setUserData(this);
    }

    /*--------------- events -------------*/
    public abstract void onCollideBegins(Identity identity);
    public abstract void onCollideEnds(Identity identity);
    protected abstract void handleStatusDrawing(float delta);
}
