package game.base.toolkits;

import android.content.Context;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import game.variables.GlobalVariable;
import gdx.android.AndroidLauncher;

public class ScoreManager {

    public static void checkRecord(int score){
        int last_record = readScore();
        if(score > last_record){
            archiveScore(score);
        }
    }

    public static void archiveScore(int score){
        try{
            FileOutputStream fos = AndroidLauncher.context.openFileOutput(GlobalVariable.FILE_RECORD, Context.MODE_PRIVATE);
            fos.write(score);
            fos.close();
            Log.i("archive score:", "score wrote "+score);
        }catch(IOException e){
            Log.e("archive score:", "writing score error");
        }
    }

    public static int readScore(){
        int record = -1;
        try{
            FileInputStream fis = AndroidLauncher.context.openFileInput(GlobalVariable.FILE_RECORD);
            record = fis.read();
            fis.close();
        }catch (IOException e){
            Log.e("read record:", "cannot read the record");
        }
        Log.i("read record:","record = "+record);
        return record;
    }
}
