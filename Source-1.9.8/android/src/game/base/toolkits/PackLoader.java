package game.base.toolkits;

import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

import java.util.HashMap;
import java.util.Iterator;

public class PackLoader {
    public static String separator;
    private XmlReader reader;

    private HashMap<String, Array<TextureAtlas.AtlasRegion>> frameList;

    public PackLoader(String pack_separator){
        separator = pack_separator;
        reader = new XmlReader();
        frameList = new HashMap<String, Array<TextureAtlas.AtlasRegion>>();
    }

    public void clear(){
        frameList.clear();
    }

    public String getSeparator(){return separator;}

    public HashMap<String, Array<TextureAtlas.AtlasRegion>> getFrameList() {return frameList;}

    public void loadPacks(FileHandle xml){
        Log.d("xmlreader","start reading pack");
        XmlReader.Element xml_element = reader.parse(xml);

        Iterator iterator_pack = xml_element.getChildrenByName("pack").iterator();

        while(iterator_pack.hasNext()){
            XmlReader.Element pack = (XmlReader.Element) iterator_pack.next();
            Log.d("reading xml", "pack is "+pack);
            String pack_name = pack.getAttribute("name");
            String pack_path = pack.getAttribute("path");


            parsePack(pack_name, pack_path);
        }
    }

    private void parsePack(String name, String path){
        TextureAtlas tmpAtlas = new TextureAtlas(Gdx.files.internal(path));
        /*------------ create animation with "name + animation_name"
         * animation name can be more than 1. So it's an array.
         */
        Array<AtlasRegion> tmpRegions = tmpAtlas.getRegions();

        String oldRegion = "";

        for(AtlasRegion region : tmpRegions){
            Log.d("adding region:", name + separator + region.name);
            if(region.name.equals(oldRegion)){
                continue;
            }
            frameList.put(name + separator + region.name, tmpAtlas.findRegions(region.name));
            oldRegion = region.name;
        }

    }
}
