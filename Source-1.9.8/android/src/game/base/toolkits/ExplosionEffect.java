package game.base.toolkits;

import android.util.Log;

import com.badlogic.gdx.math.Shape2D;

import game.base.actors.Bomb;
import game.base.actors.GameObject;
import game.variables.GlobalVariable;
import game.variables.Identity;
import game.main.compositors.ResourceCache;
import game.main.GameApp;

/**
 * Created by root on 5/7/17.
 */

public class ExplosionEffect extends Effect {
    private Identity identity;
    public static float sound_decay = 0;
    public static boolean isChainExplosion = false;

    /****************** constructor *******************/
    public ExplosionEffect(Identity identity, Shape2D shape){
        super(shape);
        this.identity = identity;
        effectSound = ResourceCache.soundsList.get("explosion");
    }

    @Override
    public void applyEffect(float delta) {
        effectSound.play(GlobalVariable.SOUND_VOLUME + sound_decay);

        for (GameObject go : GameApp.playScreen.gameManager.bombsList){
            Bomb bomb = (Bomb)go;
            /* explode if the explosion's element counters bomb's element,
               and the bomb is in the effect's shape */
            if (this.identity.counters(bomb.identity) &&
                    shape.contains(bomb.position.x, bomb.position.y)){
                bomb.explode();
                isChainExplosion = true;
            }
        }
        // when chain explosion finished
        if(!isChainExplosion) {
            Log.d("sound decay:", Float.toString(sound_decay));
            // sound_decay = 0;
            GameApp.playScreen.gameManager.wisp.regenerate();
        }
    }
}
