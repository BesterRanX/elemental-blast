package game.base.gameui.hud;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import game.base.toolkits.ScoreManager;
import game.main.GameApp;
import game.main.compositors.GameManager;
import game.main.screen.PlayScreen;

/**
 * Created by root on 11/10/17.
 */

public class HomeButton extends TextButton {
    /***************** ATTRIBUTES ******************/
    /***************** CONSTRUCTOR *******************/
    public HomeButton(TextButtonStyle style, final GameApp gapp){
        super("", style);
        addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                GameApp.playScreen.gameManager.endGame();
            }
        });
    }
}
