package game.base.gameui.hud;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import game.main.GameApp;

/**
 * Created by root on 5/20/17.
 */

public class TimeLabel extends Label {
    /*************** ATTRIBUTES *****************/
    // level effect counter
    private int level_effect_cap = 5; //each 5 seconds increase a row
    private int level_effect_counter = 0;

    // level effect incresing
    private int level_difficulty_cap = 60;
    private int level_difficulty_counter = 0;

    private int seconds = 0;
    private int minutes = 0;
    private float milliseconds = 0;


    /**************** CONSTRUCTOR *************************/
    public TimeLabel(LabelStyle style){
        super("", style);
    }


    /***************** METHODS ********************/
    @Override
    public void act(float delta) {
        super.act(delta);
        if(!GameApp.playScreen.gameManager.isPause){
            milliseconds += delta*100;

            /*------ calculate seconds ------*/
            if (milliseconds >= 60) {

                milliseconds = 0;
                seconds += 1;

                // level difficulty
                level_difficulty_counter++;
                level_effect_counter++;
            }

            /*----- calculate minutes ----*/
            if (seconds >= 60) {
                seconds = 0;
                minutes += 1;
            }

            /*---------- each 60s increase the game difficulty ----------*/
            if(level_difficulty_counter >= level_difficulty_cap){
                level_difficulty_counter = 0;
                GameApp.playScreen.gameManager.increaseDifficult();
            }

            /*---------- apply level effect -------------------*/
            if (level_effect_counter >= level_effect_cap){
                level_effect_counter = 0; // reset counter
                GameApp.playScreen.gameManager.applyLevelEffect();
            }
            setText(String.format("%2d", minutes)+"."+String.format("%2d",seconds));
        }
    }
}
