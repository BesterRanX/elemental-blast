package game.base.gameui.hud;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import game.main.GameApp;
import game.main.compositors.ResourceCache;
import game.main.screen.PlayScreen;

import static game.base.toolkits.PackLoader.separator;

/**
 * Created by root on 4/23/17.
 */

public class Generator extends Button{
    /***** final ******/
    private final float BOUND_WIDTH = 140;
    private final float BOUND_HEIGHT = 60;

    /************* ATTRIBUTES ************/
    private TextureRegion currentFrame;
    private float width, height;
    private Vector2 position;


    /************** CONSTRUCTORS ***********/
    public Generator(float posx, float posy, float width, float height){

        position = new Vector2(posx, posy);
        this.width = width;
        this.height = height;
        this.setBounds((width - BOUND_WIDTH)/2, height - BOUND_HEIGHT, BOUND_WIDTH, BOUND_HEIGHT);
        this.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                GameApp.playScreen.gameManager.wisp.shoot(GameApp.playScreen.guiManager.directionController.getRotation());
            }
        });
        initialiseTexture();
    }


    /*************** OPERATORS ****************/
    private void initialiseTexture(){
        currentFrame = ResourceCache.framesList.get("generator"+separator+"STANDBY").first();
    }


    /**************** METHODS *****************/
    @Override
    public void draw(Batch batch, float alpha) {
        batch.draw(currentFrame, position.x, position.y, width, height);
    }
}
