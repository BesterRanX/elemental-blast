package game.base.gameui.hud;

import android.util.Log;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import game.base.toolkits.ScoreManager;
import game.main.compositors.GameManager;

/**
 * Created by root on 5/20/17.
 */

public class ScoreLabel extends Label {
    private int score = 0;
    private int amount = 0;
    private ScoreManager scoreManager;
    /************* CONSTRUCTOR *****************/
    public ScoreLabel(LabelStyle style){
        super("", style);
        scoreManager = new ScoreManager();
        setText(String.format("%6d",score));
    }


    /*************** OPERATORS ***************/
    private void transferAmount(){
        if (amount > 0){
            amount -= 1;
            score += 1;
            setText(String.format("%6d",score));
        }
    }

    /*************** METHODS *****************/
    public void increaseScore(int s){
        amount += s;
    }

    public void setScore(int s){
        score = s;
        setText(String.format("%6d",score));
        Log.i("set score:","score = "+s);
    }

    public int getScore(){
        return score;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        transferAmount();
    }
}
