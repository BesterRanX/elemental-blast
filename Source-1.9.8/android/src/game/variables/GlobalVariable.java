package game.variables;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;

/**
 * Created by root on 4/15/17.
 */

/* class which contains global game varaibles */
public class GlobalVariable {
    /********** SOUNDS ***************/
    public static final float MUSIC_VOLUME = 1f;
    public static final float SOUND_VOLUME = 0.6f;

    /*********** ARCHIVE *********/
    public static String FILE_RECORD = "record";

    /********** FRAME ***********/
    public static final int FPS = 30;
    public static final float FRAME_TICK = 1/FPS;

    /********** SCREEN SIZE ********/
    public static final int VIRTUAL_SCREEN_WIDTH = 720;
    public static final int VIRTUAL_SCREEN_HEIGHT = 1420;
    public static final float LEFT_EDGE = 30;
    public static final float TOP_EDGE = GlobalVariable.VIRTUAL_SCREEN_HEIGHT - 155;
    public static final float PPM = 20;//20 pixels per meter

    /********** SPRITE SIZE *********/
    public static final int BOMB_WIDTH = 64;
    public static final int BOMB_HEIGHT = 64;
    public static final float WISP_WIDTH = 72;
    public static final float WISP_HEIGHT = 63f;
    public static final int PADDING_X = 2;
    public static final int PADDING_Y = 2;

    /************ BitMask ***********/
    public static final short BIT_WISP = 2;
    public static final short BIT_BOMB = 4;

    /******** START POINT OF WISP *******/
    public static float START_X, START_Y;

/****************************** USER INTERFACE OBJECTS ********************************************/
    /********** MAIN SCREEN BUTTONS ************/
    public static final int PLAYBUTTON_WIDTH = 330;
    public static final int PLAYBUTTON_HEIGHT = PLAYBUTTON_WIDTH/2;

    public static final int LOGINBUTTON_WIDTH = 330;
    public static final int LOGINBUTTON_HEIGHT = LOGINBUTTON_WIDTH/2;

    public static final int SCOREBOARD_WIDTH = 330;
    public static final int SCOREBOARD_HEIGHT = SCOREBOARD_WIDTH/3;

    public static final int MUTATIONBUTTON_WIDTH = 330;
    public static final int MUTATIONBUTTON_HEIGHT = MUTATIONBUTTON_WIDTH/2;

    public static final int MUTATION_ICON_WIDTH = MUTATIONBUTTON_HEIGHT/2;
    public static final int MUTATION_ICON_HEIGHT = MUTATIONBUTTON_HEIGHT/2;

    /************* GENERATOR ************/
    public static float GENERATOR_WIDTH, GENERATOR_HEIGHT;
    public static final float GENERATOR_PADDING = 10;
    public static final float GENERATOR_X = -GENERATOR_PADDING/2.5f;
    public static final float GENERATOR_Y = -10;

    /************* DEADLINE **************/
    public static final int Y_DEADLINE = Gdx.graphics.getHeight()*(VIRTUAL_SCREEN_WIDTH/VIRTUAL_SCREEN_HEIGHT) + 450;
    public static final int DEADLINE_HEIGHT = 10;

    /********* ELEMENT BUTTON POSITION *********/
    public static final float ELEMENTBUTTON_RADIUS = 112;
    public static float FIREBUTTON_X, FIREBUTTON_Y;
    public static float WATERBUTTON_X, WATERBUTTON_Y;
    public static float LEAFBUTTON_X, LEAFBUTTON_Y;
    public static float EARTHBUTTON_X, EARTHBUTTON_Y;
    private static int elementbutton_margin = 150;

    /*********** DIRECTION CONTROLLER **********/
    public static float D_CONTROLLER_X, D_CONTROLLER_Y;
    public static final float D_CONTROLLER_WIDTH = 30;
    public static final float D_CONTROLLER_HEIGHT = 220;


    public static void setupGameVariables(OrthographicCamera gameCamera){
        /*------ userinterface VARIABLES -----*/
        GENERATOR_WIDTH = VIRTUAL_SCREEN_WIDTH+GENERATOR_PADDING;
        GENERATOR_HEIGHT = GENERATOR_WIDTH/4.8f;

        /*----- WISP START POINT ----*/
        START_X = (VIRTUAL_SCREEN_WIDTH - WISP_WIDTH)/2;
        START_Y = GENERATOR_HEIGHT - (WISP_HEIGHT-10);

        /*------ element buttons ------*/
        FIREBUTTON_X = 30;
        EARTHBUTTON_X = FIREBUTTON_X + elementbutton_margin;
        WATERBUTTON_X = VIRTUAL_SCREEN_WIDTH - elementbutton_margin;
        LEAFBUTTON_X = WATERBUTTON_X - elementbutton_margin;
        EARTHBUTTON_Y = 10;
        FIREBUTTON_Y = 10;
        WATERBUTTON_Y = 10;
        LEAFBUTTON_Y = 10;

        /*----- direction controller -----*/
        D_CONTROLLER_X = (GENERATOR_WIDTH - GENERATOR_PADDING - D_CONTROLLER_WIDTH)/2;
        D_CONTROLLER_Y = GENERATOR_HEIGHT + 30;
    }
}
