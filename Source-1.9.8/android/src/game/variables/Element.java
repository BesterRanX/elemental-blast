package game.variables;

/**
 * Created by root on 4/15/17.
 */

public enum Element {
    FIRE, WATER, GRASS, EARTH, NEUTRAL
}
