package gdx.android;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.tasks.Task;

import game.main.GameApp;
import gdx.android.admob.AdHandler;
import gdx.android.admob.AdsConfig;
import gdx.game.BuildConfig;

public class AndroidLauncher extends AndroidApplication implements AdHandler{
	public static Context context;

	// ads attributes
	private final int RC_SIGN_IN = 100;
	private final int SHOW_ADS = 1;
	private final int HIDE_ADS = 0;

	// adview attributes
	private static AdView adView;
	private Handler handler;
	private boolean ads_toggle = false;
	protected AdRequest.Builder builder;
	protected RelativeLayout.LayoutParams adParams;

	// relativelayout
	protected RelativeLayout screenLayout;

	/*
	// google components
	protected GoogleSignListener googleSignListener;
	protected GoogleSignInOptions gso;
	protected GoogleSignInClient mGoogleSignInClient;
	public static GoogleSignInAccount googleAccount;
    */

	/******************** INITIALIZATION ********************/
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        context = getContext();
		MobileAds.initialize(this, BuildConfig.APPLICATION_ID);

		// initialise il layout
        layoutInitialization();

        // initialise the gameview
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		View gameView = initializeForView(new GameApp(this, null), config);
		screenLayout.addView(gameView);

		// initialise the admob
		admobInitialization();
		screenLayout.addView(adView, adParams);
		adView.loadAd(builder.build());

		//  initialise handerl
		adhandlerInitialization();
		setContentView(screenLayout);
		// initialise_gpscomponents();
	}

	/******************* ADMOB OPERATORS **********************/
	private void admobInitialization(){
		adView = new AdView(this);

		adView.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				int visiblity = adView.getVisibility();
				adView.setVisibility(AdView.GONE);
				adView.setVisibility(visiblity);
				Log.i("admob:","loaded successfully");
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				Log.e("admob:","failed loading");
			}
		});

		adView.setAdSize(AdSize.BANNER);
		adView.setAdUnitId(AdsConfig.ADMOB_ID);
		builder = new AdRequest.Builder();
		builder.addTestDevice("AC16F03452CDBA7DBA091C5A440827F5");
	}

	private void adhandlerInitialization(){
		handler = new Handler(){
			public void handleMessage(Message msg) {
				switch(msg.what){
					case SHOW_ADS:
						adView.setVisibility(View.VISIBLE);
						break;
					case HIDE_ADS:
						adView.setVisibility(View.GONE);
						break;
				}
			}
		};
	}

	public void showAds(boolean show){
		handler.sendEmptyMessage(show ? SHOW_ADS : HIDE_ADS);
	}

	public void toggleAds(){
		showAds(ads_toggle);
		ads_toggle = !ads_toggle;
	}

	/***************** LAYOUT INIT *******************************/
	private void layoutInitialization(){
		screenLayout = new RelativeLayout(this);
		screenLayout.setGravity(Gravity.BOTTOM);
		adParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT
		);
		adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		adParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
	}

	/******************** GOOGLE ACCOUNT OPERATORS ****************/

	/*
	private void initialise_gpscomponents() {
		gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestEmail()
                .requestScopes(Drive.SCOPE_FILE)
				.requestIdToken("728100313954-6vpa4ih1110vqf1fndbl2ihv34j63edt.apps.googleusercontent.com")
				.build();
		mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
	}


	@Override
	public void addGoogleSignListener(GoogleSignListener listener){
		googleSignListener = listener;
	}

	@Override
	public void SignIn(){
		Intent signInIntent = mGoogleSignInClient.getSignInIntent();
		startActivityForResult(signInIntent, RC_SIGN_IN);
	}

	@Override
	public GoogleSignInAccount getSignedAccount(){
		googleAccount = GoogleSignIn.getLastSignedInAccount(this);

		if(googleAccount != null){
			googleSignListener.onSignIn();

		}else {
			googleSignListener.onSignOut();
		}
		return googleAccount;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		// Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
		if (requestCode == RC_SIGN_IN) {
			Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
			handleSignInResult(task);
		}
		// Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
	}

	private void handleSignInResult(Task<GoogleSignInAccount> task) {
		try {
			GoogleSignInAccount account = task.getResult(ApiException.class);
            Log.d("account info:", account.getEmail());
            googleSignListener.onSignIn();

		} catch (ApiException e) {
			Log.e("account signin", e.getMessage());
			Log.e("account signin","signin failed");
			// The ApiException status code indicates the detailed failure reason.
			// Please refer to the GoogleSignInStatusCodes class reference for more information.
		}
	}
	*/
}
