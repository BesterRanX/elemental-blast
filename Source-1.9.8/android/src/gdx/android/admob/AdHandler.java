package gdx.android.admob;

import gdx.android.googleaccount.GoogleSignListener;

public interface AdHandler {
    void toggleAds();
    void showAds(boolean show);
}
