package gdx.android.googledrive;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.CreateFileActivityOptions;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveClient;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import static android.content.ContentValues.TAG;
import static android.support.v4.app.ActivityCompat.startIntentSenderForResult;
import static com.google.android.gms.drive.Drive.getDriveResourceClient;

public class DriveManager {
    private static final int REQUEST_CODE_CREATE_FILE = 2;
    private Context context;
    private GoogleSignInAccount googleaccount;

    // drive components
    protected DriveResourceClient drive_ResourceClient;
    protected DriveClient driveClient;
    private DriveFolder driveFolder;

    /******************* CONSTRUCTOR ******************/
    public DriveManager(Context context, GoogleSignInAccount account){
        drive_ResourceClient = Drive.getDriveResourceClient(context, googleaccount);
        driveClient = Drive.getDriveClient(context, googleaccount);
        initialization_Drive();
    }

    private void initialization_Drive(){

    }

    private DriveFolder exists_AppFolder(){

        Task<DriveFolder> driveFolderTask = drive_ResourceClient.getAppFolder();

        driveFolderTask
                .addOnSuccessListener(new OnSuccessListener<DriveFolder>() {
                    @Override
                    public void onSuccess(DriveFolder driveFolder) {
                        Log.d("drive folder ID:", driveFolder.getDriveId().getResourceId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("drive error", "drive does not exits");
                    }
                });

        return driveFolderTask.getResult();
    }

    public void store_FileContents(File file, Metadata metadata, DriveContents contents){

    }

    private void saveFileToDrive(final File file) {
        // Start by creating a new contents, and setting a callback.
        Log.i(TAG, "Creating new contents.");

        drive_ResourceClient
                .createContents().continueWith(
                        new Continuation<DriveContents, Object>() {
                            @Override
                            public Void then(@NonNull Task<DriveContents> task){
                                write_Contents(task.getResult(), file);
                                return null;
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "Failed to create new contents.", e);
                            }
                        });

    }



    public DriveId write_Contents(DriveContents driveContents, File file){

        OutputStream os = driveContents.getOutputStream();
        byte[] bytes = read_FileBytes(file);

        try {
            os.write(bytes);
            os.flush();
            os.close();
        }catch (Exception ex){
            Log.e("drivecontents:","cannot write");
        }
        return driveContents.getDriveId();
    }

    public byte[] read_FileBytes(File file){
        BufferedInputStream bis;
        int size = (int)file.length();
        byte[] bytes = new byte[size];

        try{
            bis = new BufferedInputStream(new FileInputStream(file));
            bis.read(bytes);
            bis.close();
        }catch (Exception ex){
            Log.e("reading file:", "error cannot read the file");

        }
        return bytes;
    }

}
