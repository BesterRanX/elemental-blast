package gdx.android.googleaccount;

public interface GoogleSignListener {
    void onSignIn();
    void onSignOut();
}
