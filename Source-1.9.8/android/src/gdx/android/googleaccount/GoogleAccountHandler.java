package gdx.android.googleaccount;

import android.content.Context;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;

public interface GoogleAccountHandler {
    void SignIn();
    GoogleSignInAccount getSignedAccount();
    void addGoogleSignListener(GoogleSignListener googleSignListener);

}
